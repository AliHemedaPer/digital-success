<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class RegisterConfirmationController extends Controller
{
    /**
     * Confirm a user's email address.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        $user = User::where('confirmation_token', request('token'))->first();
        if (! $user) {
            return redirect(route('login'))->with('flash', 'Unknown token.');
        }

        $user->confirm();
        return redirect(route('login'))->with('flash', 'Your account is now confirmed! You may login again.');
    }

    public function waitConfirm(Request $request)
    {
        return view('emails.wait-confirm', ['message' => 'Thank you: please check your email to confirm account!']);
    }
}
