<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Common\Post;
use App\Models\Common\Banner;
use App\Models\Common\Counters;
use App\Models\Common\Category;
use App\Models\Client\Client;

use App;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;

class FrontController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
    //     $locale = App::getLocale();
    //     echo $locale;
    //     exit();
        $ishome     = 1;
        $banners    = Banner::get();
        $counters   = Counters::get();
        $posts      = Post::where('post_type', 'blog')->latest()->limit(3)->get();
        /*$about      = About::find(1);
        $categories = Categories::where('in_home', '=', 1)->get();
        $services   = Service::get();
        $posts      = Posts::limit(3)->latest()->get();
        $works      = Works::limit(3)->latest()->get();
        $team       = Counters::get();
        $logos      = Integration::get();*/
        ////
        return view('front.home')->with(compact('ishome', 'banners', 'about', 'posts', 'counters'));
    }

    public function page($page_key)
    {
        $bkg = rand(1,8);
        $pages = [
                    'who_we_are' => 1, 
                    'why_us' => 2, 
                    'payments' => 3,
                    'privacy_policy' => 4,
                    'service_agreement' => 5
                 ];
        $data = Post::find($pages[$page_key]);
        $counters       = Counters::get();
        return view('front.page')->with(compact('data', 'counters', 'bkg'));
    }

    public function contact()
    {
        $bkg = rand(1,8);
        return view('front.contact')->with(compact('bkg'));
    }

    public function services(Category $category)
    {
        $bkg = rand(1,8);
        return view('front.services')->with(compact('category', 'bkg'));
    }

    public function service(Post $service)
    {
        $bkg = rand(1,8);
        return view('front.service')->with(compact('service', 'bkg'));
    }

    public function joinus()
    {
        $top_slider_img = 'slider-img-Joinus';
        return view('front.joinus')->with(compact('top_slider_img'));
    }

    public function blog()
    {
        $bkg = rand(1,8);
        $posts = Post::where('post_type', 'blog')->orderBy('updated_at', 'DESC')->get();
        return view('front.blog')->with(compact('posts', 'bkg'));
    }

    public function post(Post $post)
    {
        $bkg = rand(1,8);
        $posts = Post::where('post_type', 'blog')
                     ->where('id', '!=', $post->id)->orderBy('updated_at', 'DESC')->limit(5)->get();
        return view('front.blogpost')->with(compact('posts', 'post', 'bkg'));
    }

    public function servicemail(Request $request){

        $typ = $request->typ;

        $appSettings = DB::table('settings')->select(['app_form_email'])->where('id', 1)->first();
        $toMail = $appSettings->app_form_email;

        if($typ == 'service')
        {
            $title  = $request->title;
            $data = ['title' => $title, 'email' => $request->email, 'name' => $request->name, 'phone' => $request->phone, 'message' => $request->message];

            $mail = Mail::send('emails.service', ['data' => $data], function ($m) use ($toMail, $title) {
                $m->to($toMail)->subject($title.' - Service Request!');
            });
             $dataClient = [
                            'name'  => $request->name,
                            'email' => $request->email,
                            'phone' => $request->phone
                          ];
            try{
                Client::firstOrCreate(['email' => $request->email], $dataClient);
            }catch(Exception $e){}

        }
        else if($typ == 'contact')
        {
            $subject = $request->title;
            $data = ['email' => $request->email, 'name' => $request->name, 'phone' => $request->phone, 'subject' => $subject, 'message' => $request->message];
            $mail = Mail::send('emails.contact', ['data' => $data], function ($m) use ($toMail, $subject) {
                $m->to($toMail)->subject('Web Contact Form');
            });
        }
        


        //if($mail) {
            $msgStatus = 1;
        //}
        return response()->json([
            'status' => $msgStatus
        ]);
    }

    public function contactmail(Request $request){

        $appSettings = DB::table('settings')->select(['app_form_email'])->where('id', 1)->first();
        $toMail = $appSettings->app_form_email;

        $subject = $request->title;

        $data = ['email' => $request->email, 'name' => $request->name, 'phone' => $request->phone, 'subject' => $subject, 'message' => $request->message];
        print_r($data);
        exit();
        $mail = Mail::send('emails.contact', ['data' => $data], function ($m) use ($toMail, $subject) {
            $m->to($toMail)->subject('Web Contact Form');
        });

        return response()->json([
            'status' => 1
        ]);
    }
}
