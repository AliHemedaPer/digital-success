<?php

namespace App\Http\Controllers\V1\Admin;

use DB;
use App\Admin;
use App\Models\Client\Client;
use App\Models\Common\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function home()
    {
        $services = Post::where('post_type', 'service')->count();
        $blog     = Post::where('post_type', 'blog')->count();
        $clients  = Client::count();

        return view('admin.home', compact('services', 'blog', 'clients'));
    }

    private function getRoles($rols)
    {
        if ($rols) {
            foreach ($rols as $key => $value) {
                $rols[$key] = array_keys($value);
            }
        }
        return $rols;
    }

    public function appSettings(Request $request)
    {
        if ($request->method() == 'POST') {
            $validatedData = $request->validate([
                'app_name' => 'required|string',
                'app_email' => 'required|string',
                'app_form_email' => 'required',
                'app_phone' => 'required',
            ]);

            $updates = [
                'app_name'   => $request->app_name,
                'app_email'   => $request->app_email,
                'app_form_email' => $request->app_form_email,
                'app_phone' => $request->app_phone,
                'app_address_en' => $request->app_address_en ?? '--',
                'app_address_ar' => $request->app_address_ar ?? '--',
                'fb'   => $request->fb ?? '',
                'tw'   => $request->fb ?? '',
                'in'   => $request->fb ?? ''
            ];

            $file_name = '';
            if ($request->hasFile('image')) {
                $path = '/public/images/settings/';
                $file_name = time();
                $ext = $request->image->getClientOriginalExtension();
                // If Invalid Extension
                if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png'])) {
                    $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                    $request->session()->flash('flash_type', 'alert-danger');
                    return back();
                }

                $fname = $request->image->getClientOriginalName();
                $file_name = time();
                $file_name .= $file_name . $fname;
                $request->image->move(base_path() . $path, $file_name);
                $updates['image'] = $path . $file_name;
            }

            if ($validatedData) {
                DB::table('settings')->where('id', 1)->update($updates);
                return redirect('admin/settings');
            }
        }
        $settings = DB::table('settings')->first();
        return view('admin.settings', compact('settings'));
    }

    public function changePassword(Request $request)
    {
        $id = auth('admin')->id();
        $admin = Admin::find($id);

        if ($request->method() == 'POST') {
            $messages = [
              'same' => 'New Password and Confirmation password miss match!',
            ];
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|max:191',
                'npassword' => 'required|string|max:191',
                'cpassword' => 'required|string|max:191|same:npassword'
           ], $messages);

            $validator->validate();

            if(!Hash::check($request->password, $admin->password)){
                $validator->errors()->add('field', 'Current password incorrect!');
                $errors = $validator->errors()->all();
                return Redirect('admin/chpassword')->withErrors($errors);
            }

            $admin->update(['password' => Hash::make($request->npassword)]);
            return Redirect('admin/home');
        }
        
        return view('admin.chpassword');
    }

    public function clients(){
      $clients = Client::all();
      return view('admin.clients')->with(compact('clients'));
    }

    public function clientSetStatus(Client $client, $status){
      $client->update(['status' => $status]);
      return response()->json(['status' => 1]);
    }
    
}
