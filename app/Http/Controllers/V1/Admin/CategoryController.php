<?php

namespace App\Http\Controllers\V1\Admin;

use App\Models\Common\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = Category::all();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $parents = Category::all();
        return view('admin.categories.create', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name_en' => 'required|string|max:255',
                    'name_ar' => 'required|string|max:255'
        ]);

        $validator->validate();

        $file_name = '';
        if ($request->hasFile('image')) {
            $path = base_path() . '/public/images/categories/';
            $file_name = time();
            $ext = $request->image->getClientOriginalExtension();
            // If Invalid Extension
            if (!in_array($ext, ['jpg', 'JPG', 'JPEG', 'PNG', 'jpeg', 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $fname = $request->image->getClientOriginalName();
            $file_name = time();
            $file_name .= $file_name . $fname;
            $request->image->move($path, $file_name);
        }
        Category::create([
                    'parent' => $request->input('parent') ?? 0,
                    'name_en' => $request->input('name_en'),
                    'name_ar' => $request->input('name_ar'),
                    'content_en' => $request->input('content_en'),
                    'content_ar' => $request->input('content_ar'),
                    'image' => $file_name,
                    'home_page' => $request->has('home_page')?1:0,
            ]);
        $request->session()->flash('success', 'Created Successfully');
        return Redirect::action('V1\Admin\CategoryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) {
        $parents = $parents = Category::where('id', '!=', $category->id)->get();
        return view('admin.categories.edit', compact('parents', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function update(Category $category, Request $request) {
        $validator = Validator::make($request->all(), [
                    'name_en' => 'required|string|max:255',
                    'name_ar' => 'required|string|max:255'
        ]);

        $validator->validate();

        if ($request->hasFile('image')) {
            $path = base_path() . '/public/images/categories/';
            $file_name = time();
            $ext = $request->image->getClientOriginalExtension();
            // If Invalid Extension
            if (!in_array($ext, ['jpg', 'JPG', 'JPEG', 'PNG', 'jpeg', 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $fname = $request->image->getClientOriginalName();
            $file_name = time();
            $file_name .= $file_name . $fname;
            $request->image->move($path, $file_name);            
            if($category->image && \File::exists($path . $category->image)) {
                unlink($path . $category->image);
            }
        }
        else{
            $file_name = $category->image;
        }
        $category->update([
            'parent' => $request->input('parent') ?? 0,
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'content_en' => $request->input('content_en'),
            'content_ar' => $request->input('content_ar'),
            'image' => $file_name,
            'home_page' =>  $request->has('home_page')?1:0,
        ]);
        $request->session()->flash('success', 'Upated Successfully');
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {
        $category->delete();
        return response()->json(['status' => 1]);
    }
}
