<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Models\Common\Post;
use App\Models\Common\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $post_type = $request->get('pst');
        if(!$post_type)
            die('no post type');

        $page_title = 
        $posts = Post::where('post_type', $post_type)->get();
        return view('admin.post.index', compact('posts', 'post_type'));
    }

    public function create(Request $request)
    {
        $post_type = $request->get('pst') ?? '';
        if(!$post_type)
            die('no post type');

        $categories = Category::all();
        return view('admin.post.form', compact('categories', 'post_type'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255',
            //'category' => 'required|numeric',
            'content_en' => 'required',
            'content_en' => 'required'
       ]);

        $validator->validate();
        $post_type = $request->post_type;
        $data = $request->all();

        $file_name = '';
        if ($request->hasFile('image')) {
            $path = '/public/images/posts/';
            $file_name = time();
            $ext = $request->image->getClientOriginalExtension();
            // If Invalid Extension
            if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $fname = $request->image->getClientOriginalName();
            $file_name = time();
            $file_name .= $file_name . $fname;
            $request->image->move(base_path() . $path, $file_name);
            $data['image'] = $path.$file_name;
        }

        $post = Post::create($data);

        $request->session()->flash('success', 'Created Successfully');
        return redirect()->route('posts.index', ['pst' => $post_type]);
        //return Redirect::action('V1\Admin\PostController@index');
    }


    public function edit(Request $request, Post $post)
    {
        $post_type = $request->get('pst');
        
        if(!$post_type)
            return redirect('admin/home');

        $categories = Category::all();
        return view('admin.post.form', compact('post', 'categories', 'post_type'));
    }

    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255',
            //'category' => 'required|numeric',
            'content_en' => 'required',
            'content_en' => 'required'
       ]);

        $validator->validate();
        $post_type = $request->post_type;
        $updates = $request->all();

        $file_name = '';
        if ($request->hasFile('image')) {
            $path = '/public/images/posts/';
            $file_name = time();
            $ext = $request->image->getClientOriginalExtension();
            // If Invalid Extension
            if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png'])) {
                $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                $request->session()->flash('flash_type', 'alert-danger');
                return back();
            }

            $fname = $request->image->getClientOriginalName();
            $file_name = time();
            $file_name .= $file_name . $fname;
            $request->image->move(base_path() . $path, $file_name);
            $updates['image'] = $path.$file_name;
        }

        $post->update($updates);
        $request->session()->flash('success', 'Upated Successfully');

        if($post_type == 'page')
            return redirect('admin/home');
        
        return redirect()->route('posts.index', ['pst' => $post_type]);
    }

    /*public function toggleStatus(Post $post)
    {
        $newSts = $post->status ? 0 : 1;
        $post->update(['status' => $newSts ]);

        return response()->json(['status' => 1]);
    }*/

    public function destroy(Post $post)
    {
        $post->delete();
        return response()->json(['status' => 1]);
    }    
}
