<?php
namespace App\Http\Controllers\V1\Client;

use App\Models\Client\Client;
use App\Models\Common\Pharmacy;
use App\Models\Common\Offer;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

header("Access-Control-Allow-Origin: *");

class ClientController extends Controller
{
    /**
     * API to obtain available User Type Other Than Admin
     */
    public function getObtainUserType()
    {
        try {
            // Model To Obtain User Type
            $user_type = UserType::where('user_type', '!=', 'admin')->get();
            foreach ($user_type as $type) {
                $type_array[] = ['id' => $type->id , 'type' => $type->user_type];
            }
            $response_array = ['status' => 'SUCCESS' , 'msg' => 'User types Obtained !' , 'data' => $type_array];

            return Response()->json($response_array);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     *
     * @param int $is_Web
     *
     * @return mixed
     */
    public function CreateUser(Request $request, $is_Web = 0)
    {
        //      if (!$this->isCsrfAccepted ()) {
        //          return Response()->make(['status' => 'FAILURE' , 'msg' => 'Invalid Request made'] , 401);
//
        //      }
        try {
            $email = $request->email ?? '';
            $phone = $request->phone ?? '';
            $address = $request->address ?? '';
            $password = $request->password ?? '';
            $confirm_password = $request->confirm_password ?? '';
            $user_type = 3;

            $ex = Client::where('email', '=', $email)->count();
            echo $ex;
            exit;
            if (Client::where('email', '=', $email)->count() != 0) {
                echo 'hii';
                return Response()->make(['status' => 'FAILURE' , 'msg' => 'Some techinical issues has occured'], 409);
            }


            //check if password == confirm password(not needed for mobile), then proceed the following
            $digits = 4;
            $randomValue = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

            if ($is_Web) {
                $name = $request->user_name ?? '';
                $client = new Client;
                $client->mail = $email;
                $client->phone = $phone;
                $client->address = $address;
                $client->first_name = $name;
                $client->save();
                $userId = $client->id;
            } else {
                $client = new Client;
                $client->mail = $email;
                $client->phone = $phone;
                $client->address = $address;
                $client->created_at = date('Y-m-d H:i:s');
                $client->save();
                $userId = $client->id;
            }

            // $user = new User;
            // $user->email = $email;
            // $user->password = Hash::make($password);
            // $user->phone = $phone;
            // $user->user_type_id = $user_type;
            // $user->user_id = $userId;
            // $user->security_code = $randomValue;
            // $user->save();
            // $postData = [
            //     [
            //         'result' => [
            //             'status' => 'success'
            //         ]
            //     ]
            // ];
            $path = base_path() . '/public/images/prescription/' . $email;
            File::makeDirectory($path, $mode = 0777, true, true);
            try {
                // if ($is_Web == 0) {
                //  Mail::send ('contact.display' , array('code' => $randomValue) , function ($message) use ($email) {
                //      $message->to ($email)->subject ('Activate Account');
                //  });
                // } else {
                //  Mail::send ('emails.register' , array('name' => $name , 'user_name' => $email , 'pwd' => $password , 'code' => $randomValue) , function ($message) use ($email) {
                //      $message->to ($email)->subject ('Activate Account');
                //  });
                // }
            } catch (Exception $e) {
                return Response()->make(['status' => 'FAILURE' , 'msg' => 'Some techinical issues has occured'], 500);
            }


            //else
            return Response()->json(['status' => 'SUCCESS' , 'msg' => 'Acccount successfully created, Please check mail for the code'], 201);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * Update User Details
     *
     * @param int $isWeb
     *
     * @return mixed
     */
    public function profileUpdate(Request $request, $isWeb = 0)
    {
        if ($isWeb) {
            $oldemail = auth('client')->user()->email;
            $email = $request->email ?? '';
            $name = $request->name ?? '';
            $phone = $request->phone ?? 00;
            $gender = $request->gender;
            $bdate = $request->bdate;
        }

        $clientDetails = ['name' => $name ,
                          'phone' => $phone ,
                          'email' => $email,
                          'gender' => $gender,
                          'bdate' => $bdate
                         ];
        $affectedRows = Client::where('email', '=', $oldemail)->update($clientDetails);

        if (count($affectedRows) == 1) {
            $result = ['status' => 'SUCCESS' , 'msg' => 'Profile updated !'];
        } else {
            $result = ['status' => 'FAILED' , 'msg' => 'Profile update fail!'];
        }

        return Response()->json($result);
    }

    /**
     * User Login
     *
     * @param int $isWeb
     *
     * @return mixed
     */
    public function UserLogin(Request $request, $isWeb = 0)
    {
        try {
            $email = $request->email ?? '';
            $password = $request->password ?? '';
            if ($isWeb) {
                // if (!$this->isCsrfAccepted ()) {
                //  $result = array(array('result' => array('status' => 'failure')));
                //  return Response()->json($result);
                // }

                //$status='active';
                $status = DB::table('users')->select('user_status as status')->where('email', '=', $email)->first();
                if (!empty($status)) {
                    if ($status->status == 'PENDING') {
                        $result = [['result' => ['status' => 'pending']]];

                        $request->session()->put('user_password', $password);
                    } elseif ($status->status == 'ACTIVE') {
                        if (Auth::attempt(['email' => $email , 'password' => $password])) {
                            $request->session()->put('user_id', $email);
                            if ($request->session()->get('medicine') != "") {
                                $result = [['result' => ['status' => 'success' , 'page' => 'yes']]];
                            } else {
                                $result = [['result' => ['status' => 'success' , 'page' => 'no']]];
                            }
                        } else {
                            $result = [['result' => ['status' => 'failure']]];
                        }
                    } else {
                        $result = [['result' => ['status' => 'delete']]];
                    }
                } else {
                    $result = [['result' => ['status' => 'failure']]];
                }
            } else {
                if (Auth::attempt(['email' => $email , 'password' => $password])) {
                    $status = User::where('email', '=', $email)->join('user_status as us', 'us.id', '=', 'user_status')->first()->name;
                    Session::put('user_id', $email);
                    $pres_status = PrescriptionStatus::status();
                    $invoice_status = InvoiceStatus::status();
                    $payment_status = PayStatus::status();
                    $shipping_status = ShippingStatus::status();
                    $result = ['status' => 'SUCCESS' , 'msg' => 'User Logged In' , 'data' => ['status' => $status , 'pres_status' => $pres_status ,
                        'invoice_status' => $invoice_status , 'payment_status' => $payment_status , 'shipping_status' => $shipping_status]];
                } else {
                    throw new Exception('Invalid Login Credientials', 401);
                }
            }
            return Response()->json($result);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * Active user account
     *
     * @return mixed
     */
    public function anyActivateAccount()
    {
        try {
            $email = Input::get('email', '');
            $user = User::where('email', '=', $email)->first();
            if (is_null($user)) {
                throw new Exception('No user found !', 404);
            }

            $sec_code = Input::get('security_code', '');
            $securityCode = $user->security_code;
            if (str_is($securityCode, $sec_code)) {
                $updatedValues = ['user_status' => UserStatus::ACTIVE()];
                User::where('email', '=', $email)->update($updatedValues);
                $pass = Session::get('user_password');
                Auth::attempt(['email' => $email , 'password' => $pass]);
                Session::put('user_id', $email);
                $result = ['status' => 'SUCCESS' , 'msg' => 'Your account has been successfully activated !'];
            } else {
                throw new Exception('Invalid activation code', 400);
            }

            return Response()->json($result);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * function to activate the user registration from web
     *
     * @param $code
     *
     * @return mixed
     */
    public function activateAccount($code)
    {
        $client = Client::where('security_code', '=', $code)->first();
        if (count($client)) {
            $updatedValues = ['security_code' => '', 'status' => 1];
            $client->update($updatedValues);

            return Redirect('/?msg=success');
        } else {
            return Redirect('/?msg=failed');
        }
    }

    /**
     * Get User Details
     *
     * @return mixed
     */
    public function anyUserDetails()
    {
        try {
            if (!Auth::check()) {
                throw new Exception('you are not authorised', 401);
            }

            $email = Input::get('email', '');


            if (empty($email)) {
                throw new Exception('Email field is empty', 400);
            }

            $user = User::where('email', '=', Auth::user()->email)->first();
            if ($user != null) {
                if ($user->user_type_id == UserType::CUSTOMER()) {
                    $customer = Customer::where('mail', '=', Auth::user()->email)->first();
                    $Details = ['first_name' => $customer->first_name ,
                        'last_name' => $customer->last_name ,
                        'address' => $customer->address ,
                        'phone' => $customer->phone ,
                        'type_user' => UserType::CUSTOMER() ,
                        'pincode' => $customer->pincode
                    ];
                } elseif ($user->user_type_id == UserType::MEDICAL_PROFESSIONAL()) {
                    $professional = MedicalProfessional::where('prof_mail', '=', Auth::user()->email)->first();
                    $Details = ['first_name' => $professional->prof_first_name ,
                        'last_name' => $professional->prof_last_name ,
                        'address' => $professional->prof_address ,
                        'phone' => $professional->prof_phone ,
                        'type_user' => UserType::CUSTOMER() ,
                        'pincode' => $professional->prof_pincode
                    ];
                }
                //              $result = array(array('result' => array('status' => 'success' , 'msg' => $Details)));
                $result = ['status' => 'SUCCESS' , 'msg' => 'User details obtained !' , 'data' => $Details];
            } else {
                throw new Exception('No User Details Found', 404);
            }

            //$result = array(array('result'=>$Details));
            return Response()->json($result);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * Reset Password
     *
     * @return mixed
     */
    public function resetPassword(Request $request)
    {
            $email = $request->email ?? '';
            if ($request->email && $request->security_code && $request->new_password) {
                $security_code = $request->security_code;
                $password = $request->new_password;
                $confirm_password = $request->confirm_password;
                $client = Client::where('email', '=', $email)->where('security_code', '=', $security_code)->first();
                if (!is_null($client)) {
                    $client->password = Hash::make($password);
                    $client->status = 1;
                    $client->save();
                    //                  $result = array(array('result' => array('status' => 'success')));
                    $result = ['status' => 'SUCCESS' , 'msg' => 'Password Changed'];
                    return Response()->json($result);
                }
            }else if ($request->email && auth('client')->check() && $request->new_password) {
                if(auth('client')->user()->email == $email){
                    $password = $request->new_password;
                    $confirm_password = $request->confirm_password;
                    $client = Client::where('email', '=', $email)->first();
                    if (!is_null($client)) {
                        $client->password = Hash::make($password);
                        //$client->status = 1;
                        $client->save();
                        //                  $result = array(array('result' => array('status' => 'success')));
                        $result = ['status' => 'SUCCESS' , 'msg' => 'Password Changed'];
                        return Response()->json($result);
                    }
                }
            } else {
                if (Client::where('email', '=', $email)->count() == 1) {
                    $digits = 8;
                    $randomValue = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                    $updatedValues = ['security_code' => $randomValue];
                    $client = Client::where('email', '=', $email)->update($updatedValues);
                    $data = ['email' => $email, 'app_name' => 'Saidalista', 'code' => $randomValue, 'guard' => ''];
                    Mail::send('emails.reset_password', ['data' => $data], function ($message) use ($email) {
                        $message->to($email)->subject('Reset Password');
                    });
                    //                  $result = array(array('result' => array('status' => 'reset_success')));
                    $result = ['status' => 'SUCCESS' , 'msg' => 'Password Reset'];
                    return Response()->json($result);
                }
                
            }
            return Response()->make(['status' => 'FAILURE' , 'msg' => 'No User Found'], 404);
    }

    /**
     * Check out username
     */
    public function CheckUserName(Request $request)
    {
        try {
            $current_mail = $request->u_name;
            $regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^";
            if (preg_match($regex, $current_mail)) {
                $name = DB::table('clients')->where('email', $current_mail)->pluck('email');
                if (count($name) > 0) {
                    throw new Exception('Email Already Exists !', 409);
                }
            } else {
                throw new Exception('Email is not valid !', 400);
            }

            return Response()->json(['status' => 'SUCCESS' , 'msg' => 'Email is valid'], 200);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * Account Page
     *
     * @return mixed
     */
    public function getAccountPage()
    {
        $clinet_id = auth('client')->id();
        $user_data = Client::find($clinet_id);

        if($user_data->bdate)
            $bdate = explode('-', $user_data->bdate);
        else
            $bdate = [0,0,0];

        return view('client.profile')->with(compact('user_data', 'bdate'));
    }

    /**
     * Pharmacy Page
     *
     * @return mixed
     */
    public function getPharmacy(Pharmacy $pharmacy)
    {
        //$offers = $pharmacy->offers;
        $offers = Offer::where('pharmacy_id', '=', $pharmacy->id)->where('status', '=', 1)->get();
        return view('client.pharmacy')->with(compact('pharmacy', 'offers'));
    }

    /**
     * Offer Page
     *
     * @return mixed
     */
    public function getOffer(Offer $offer)
    {
        //$offers = $pharmacy->offers;
        //$offers = Offer::where('pharmacy_id', '=', $pharmacy->id)->where('status', '=', 1)->get();
        return view('client.offer')->with(compact('offer'));
    }
    

    /**
     * Change Password
     *
     * @return int
     */
    public function anyChangePassword()
    {
        $old_password = Input::get('old_password');
        $new_password = Input::get('new_password');
        $re_password = Input::get('re_password');
        $pass = Hash::make($new_password);
        $current_password = Auth::user()->password;
        $name = Auth::user()->customer->first_name;
        if (Hash::check($old_password, $current_password)) {
            if ($new_password == $re_password) {
                Auth::user()->password = $pass;
                $updt = Auth::user()->save();
                if ($updt) {
                    Mail::send('emails.change_password', ['name' => $name], function ($message) {
                        $message->to(Auth::user()->email)->subject('Your ' . Setting::param('site', 'app_name')['value'] . ' password has been changed');
                    });

                    return 1;  ///password updated
                } else {
                    return 3;  ///password could't updated
                }
            } else {
                return 2; ///password missmatch
            }
        } else {
            return 0;  ///old password error
        }
    }

    /**
     * Store profile pic
     */
    public function anyStoreProfilePic()
    {
        if (!$this->isCsrfAccepted()) {
            return 0;
        }
        $email = Auth::user()->email;
        $path = base_path() . '/public/images/prescription/' . $email . '/';
        $fname = Input::file('file')->getClientOriginalName();
        $ext = Input::file('file')->getClientOriginalExtension();
        $path_from = Input::file('file')->getRealPath();
        $newName = "profile_pic";
        $realpath = $path . "/" . $fname;
        // open an image file
        $img = Image::make($path_from);
        // now you are able to resize the instance
        $img->resize(200, 200);
        // finally we save the image as a new file
        $img->save($path . '/' . $newName);

        return Redirect::back();
    }

    /**
     * Contact Email
     *
     * @return int
     */
    public function anyContactUs()
    {
        if (!$this->isCsrfAccepted()) {
            return 0;
        }
        $client_name = Input::get('name');
        $client_mail = Input::get('email');
        $client_msg = Input::get('msg');
        $mail_id = Setting::param('site', 'mail')['value'];
        Mail::send('emails.customer_query', ['client_name' => $client_name , 'client_mail' => $client_mail , 'client_msg' => $client_msg], function ($message) use ($mail_id) {
            $message->to($mail_id)->subject('Customer Query');
        });
        if (count(Mail::failures()) > 0) {
            $errors = 0; //Failed to send email, please try again
            return $errors;
        } else {
            return 1;
        }
    }

    /**
     * Check the login status
     *
     * @return mixed
     */
    public function getCheckSession()
    {
        if (auth('client')->check()) {
            $login = 1;
        } else {
            $login = 0;
        }

        return Response()->json($login);
    }

    /**
     * API for mobile devices to save or login if they are using facebook to login
     * Parameters : email,first name, lastname and facebook id
     */
    public function postFacebookLogin()
    {
        try {
            $data = Input::all();
            // Check Form Variable Is Empty
            if (empty($data) || $this->isFormVariableEmpty($data, [])) {
                return Response()->json(['status' => 0 , 'msg' => 'Some Fields Are Empty']);
            }
            $email = $data['email'];
            $fb = $data['facebook_id'];
            $first_name = $data['first_name'];
            $last_name = $data['last_name'];
            // Get Mechanic Count
            try {
                $count = User::where('email', '=', $email)->select('id', 'user_type_id', 'user_id')->first();
                if (count($count) > 0) {
                    if ($count->user_type_id == 3) {
                        $count = Customer::where('mail', '=', $email)->where('facebook_id', '=', $fb)->count();
                        if ($count > 0) {
                            $id = $count->id;
                            Auth::loginUsingId($id);

                            //$this->clearSession();
                            return Response()->json(['status' => 1 , 'msg' => 'Success']);
                        } else {
                            $status = Customer::where('id', '=', $count->user_id)->update(['facebook_id' => $fb , 'updated_on' => date('Y-m-d H:i:s')]);
                            if ($status) {
                                $id = $count->id;
                                Auth::loginUsingId($id);

                                //$this->clearSession();
                                return Response()->json(['status' => 1 , 'msg' => 'Success']);
                            } else {
                                return Response()->json(['status' => 0 , 'msg' => 'Facebook id updation failed']);
                            }
                        }
                    } elseif ($count->user_type_id == 2) {
                        return Response()->json(['status' => 0 , 'msg' => 'Email Already Exists']);
                    }
                } else {
                    // Insert Details
                    $obj = new Customer();
                    $obj->first_name = $first_name;
                    $obj->last_name = $last_name;
                    $obj->mail = $email;
                    $obj->facebook_id = $fb;
                    $obj->created_on = date('Y-m-d H:i:s');
                    $obj->updated_on = date('Y-m-d H:i:s');
                    $obj->save();
                    // Get User Id
                    $user_id = $obj->id;
                    //Create a security code
                    $digits = 4;
                    $randomValue = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                    $obj1 = new User();
                    $obj1->email = $email;
                    $obj1->user_type_id = 3;
                    $obj1->security_code = $randomValue;
                    $obj1->user_id = $user_id;
                    $obj1->created_on = date('Y-m-d H:i:s');
                    $obj1->updated_on = date('Y-m-d H:i:s');
                    $status = $obj1->save();
                    if ($status) {
                        Auth::loginUsingId($obj1->$obj1);

                        return Response()->json(['status' => 1 , 'msg' => 'Success']);
                    } else {
                        return Response()->json(['status' => 0 , 'msg' => 'User Registration Failed']);
                    }
                }
            } catch (Exception $e) {
                throw new Exception('INTERNAL SERVER ERROR:' . $e->getMessage(), 500);
            }
        } catch (Exception $e) {
            throw new Exception('INTERNAL SERVER ERROR:' . $e->getMessage(), 500);
        }
    }
}
