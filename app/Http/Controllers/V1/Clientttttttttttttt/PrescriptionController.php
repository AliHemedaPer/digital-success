<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\Controller;
use App\Models\Client\Prescription;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrescriptionController extends Controller
{
    /**
     * Store Prescription
     *
     * @param int $isWeb
     *
     * @return mixed
     */
    public function storePrescription(Request $request, $isWeb = 0)
    {
        if ($isWeb) {
            if (auth('client')->check()) {
                $email = auth('client')->user()->email;
                $client_id = auth('client')->id();
                $path = base_path() . '/public/images/prescription/' . $email . '/';
                $file_name = "";
                if ($request->hasFile('file')) {
                    $file_name = time();
                    $ext = $request->file->getClientOriginalExtension();
                    // If Invalid Extension
                    if (!in_array($ext, ['jpg' ,'JPG' ,'JPEG' ,'PNG' , 'jpeg' , 'png'])) {
                        $request->session()->flash('flash_message', 'Invalid file uploaded, Please upload jpg or png images');
                        $request->session()->flash('flash_type', 'alert-danger');
                        return back();
                    }

                    $fname = $request->file->getClientOriginalName();
                    $file_name = time();
                    $file_name .= $file_name . $fname;
                    $request->file->move($path, $file_name);
                    $newName = "thumb_" . $file_name . $fname;


                //$realpath = $path . "/" . $file_name;
                    // open an image file
                   // $img = Image::make ($realpath);
                    // now you are able to resize the instance
                    //$img->resize (60 , 60);
                    // finally we save the image as a new file
                    //$img->save ($path . '/' . $newName);
                } else {
                    $request->session()->flash('flash_message', 'Please select a file to upload');
                    $request->session()->flash('flash_type', 'alert-danger');
                    return back();
                }

                // Save Prescription
                Prescription::create([
                    'client_id' => $client_id,
                    'path' => $file_name,
                    'status' => 'pending'
                ]);

                // $pres_id = $prescription->id;
                // $invoice = new Invoice;
                // $invoice->pres_id = $pres_id;
                // $invoice->user_id = $user_id;
                // $invoice->created_at = date ('Y-m-d h:i:s');
                // $invoice->created_by = $user_id;
                // $invoice->save ();
                // $invoice_id = $invoice->id;


                $data['email'] = $email;
                $name = auth('client')->user()->name;

                // Mail::send ('emails.prescription_upload' , array('name' => $name) , function ($message) use ($data) {
                //     $message->to ($data['email'])->subject ('New order has been submitted to ' . Setting::param ('site' , 'app_name')['value']);
                // });

                // Mail::send ('emails.admin_prescription_upload' , array('name' => $name) , function ($message) use ($data) {
                //     $message->to (Setting::param ('site' , 'mail')['value'])->subject ('New prescription uploaded to ' . Setting::param ('site' , 'app_name')['value']);
                // });


                $request->session()->flash('flash_message', '<b>Success !</b> Your order has been requested successfully. Please track the status in MY PRESCRIPTIONS.');
                $request->session()->flash('flash_type', 'alert-success');

                return back();
            } else {
                return redirect('home');
            }
        } else {
            try {
                if (!Auth::check()) {
                    throw new Exception("You are not authorized to do this action", 401);
                }

                $email = Auth::user()->email;

                $prescription = Input::get('prescription', '');
                $is_pres_required = Input::get('is_pres_required', 1);

                if (empty($email)) {
                    throw new Exception('Email is empty', 400);
                }


                if ($is_pres_required && empty($prescription)) {
                    throw new Exception('Prescription is required for this order', 412);
                }

                $path = base_path() . '/public/images/prescription/' . $email . '/';
                $date = new DateTime();

                $file_name = "";

                if (!empty($prescription)) {
                    $file_name = $date->getTimestamp();
                    $img = str_replace('data:image/png;base64,', '', $prescription);
                    $file_store = file_put_contents($path . $file_name, base64_decode($img));
                    if (!$file_store) {
                        throw new Exception('File Not saved !', 403);
                    }
                }


                $prescription = new Prescription;
                $user_id = User::where('email', '=', $email)->first()->id;
                $prescription->path = $file_name;
                $prescription->created_at = date('Y-m-d H:i:s');
                $prescription->user_id = $user_id;
                $prescription->created_by = $user_id;
                $prescription->save();

                if (!empty($prescription_thumb)) {
                    $prescription_thumb = Input::get('prescription_thumb', '');
                    $img_thumb = str_replace('data:image/png;base64,', '', $prescription_thumb);
                    file_put_contents($path . $file_name . '_thumb', base64_decode($img_thumb));
                }

                $cart_length = intval(Input::get('cart_length', 0));

                $pres_id = $prescription->id;
                $invoice = new Invoice;
                $invoice->pres_id = $pres_id;
                $invoice->user_id = $user_id;
                $invoice->created_at = date('Y-m-d h:i:s');
                $invoice->created_by = $user_id;
                $invoice->save();
                $invoice_id = $invoice->id;

                if ($cart_length > 0) {
                    for ($i = 0; $i < $cart_length; $i++) {
                        $item_id = Input::get('id' . $i, null);
                        $quantity = Input::get('quantity' . $i, null);
                        if (is_null($item_id) || empty($item_id) || is_null($quantity)) {
                            continue;
                        }

                        $medicine_details = Medicine::medicines($item_id);

                        // Medicine Details
                        if (is_null($medicine_details) || empty($medicine_details)) {
                            continue;
                        }

                        $total_discount = $medicine_details['discount'] * $quantity;
                        $total_price = ($medicine_details['mrp'] * $quantity) - $total_discount;
                        $itemList = new ItemList;
                        $itemList->invoice_id = $invoice_id;
                        $itemList->medicine = $item_id;
                        $itemList->quantity = $quantity;
                        $itemList->unit_price = $medicine_details['mrp'];
                        $itemList->discount_percentage = $medicine_details['discount'];
                        $itemList->discount = $total_discount;
                        $itemList->total_price = $total_price;
                        $itemList->created_at = date('Y-m-d H:i:s');
                        $itemList->created_by = $user_id;
                        $itemList->save();
                    }
                }

                $data['email'] = $email;
                Mail::send('emails.admin_prescription_upload', ['name' => ''], function ($message) use ($data) {
                    $message->to(Setting::param('site', 'mail')['value'])->subject('New prescription uploaded to ' . Setting::param('site', 'app_name')['value']);
                });

                return Response::json(['status' => 'SUCCESS' , 'msg' => 'Your order has been requested successfully ']);
            } catch (Exception $e) {
                $message = $this->catchException($e);
                return Response::make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
            }
        }
    }

    /**
     * Get Prescription List
     *
     * @param int $is_category
     *
     * @return mixed
     */
    public function getMyPrescription($is_category = 0)
    {
        $email = auth('client')->user()->email;
        $path = URL('/') . '/public/images/prescription/' . $email . '/';
        $client_id = auth('client')->user()->id;
        //$invoices = Invoice::where ('user_id' , '=' , $user_id)->get ();
        //$prescriptions = Prescription::select ('i.*' , 'prescription.status' , 'prescription.path' , 'prescription.id as pres_id' , 'prescription.created_at as date_added')->where ('prescription.user_id' , '=' , $user_id)->where ('is_delete' , '=' , 0)->join ('invoice as i' , 'i.pres_id' , '=' , DB::raw ("prescription.id AND i.payment_status IN (" . PayStatus::PENDING () . ",0) "));
//                ->whereIn('i.payment_status', [PayStatus::PENDING(), 0]);

        $prescriptions = Prescription::where('client_id', '=', $client_id);
        $responses = [];
        switch ($is_category) {
            case ('pending'):
                $prescriptions = $prescriptions->where('status', '=', 'pending');
                break;
            case('verified'):
                $prescriptions = $prescriptions->where('status', '=', 'verified');
                break;
            case('unverified'):
                $prescriptions = $prescriptions->where('status', '=', 'unverified');
                break;
            default:
                break;
        }
        $results = $prescriptions->get();

        /*foreach ($results as $result) {
            $items = [];
            $medicines = Medicine::medicines ();
            if (!is_null ($result->id) || !empty($result->id)) {
                $carts = ItemList::where ('invoice_id' , '=' , $result->id)->get ();
                foreach ($carts as $cart) {
                    $items[] = ['id' => $cart->id ,
                        'item_id' => $cart->medicine ,
                        'item_code' => $medicines[$cart->medicine]['item_code'] ,
                        'item_name' => $medicines[$cart->medicine]['item_name'] ,
                        'unit_price' => $cart->unit_price ,
                        'discount_percent' => $cart->discount_percentage ,
                        'discount' => $cart->discount ,
                        'quantity' => $cart->quantity ,
                        'total_price' => $cart->total_price
                    ];
                }
            }
            $details = [
                'id' => (is_null ($result->id)) ? 0 : $result->id ,
                'invoice' => (is_null ($result->invoice)) ? 0 : $result->invoice ,
                'sub_total' => (is_null ($result->sub_total)) ? 0 : $result->sub_total ,
                'discount' => (is_null ($result->discount)) ? 0 : $result->discount ,
                'tax' => (is_null ($result->tax)) ? 0 : $result->tax ,
                'shipping' => (is_null ($result->shipping)) ? 0 : $result->shipping ,
                'total' => (is_null ($result->total)) ? 0 : $result->total ,
                'created_on' => (is_null ($result->date_added)) ? 0 : $result->date_added ,
                'cart' => $items ,
                'shipping_status' => (is_null ($result->shipping_status)) ? 0 : $result->shipping_status ,
                'pres_status' => $result->status ,
                'invoice_status' => is_null ($result->status_id) ? 0 : $result->status_id ,
                'path' => $result->path
            ];
            $responses[] = $details;
        }
        $payment_mode = Setting::select ('value')->where ('group' , '=' , 'payment')->where ('key' , '=' , 'mode')->first ();*/


        return View('client.my_prescription', ['prescriptions' => $results , 'email' => $email , 'cat' => $is_category , 'payment_mode' => '$payment_mode->value' , 'default_img' => url('/') . "/assets/images/no_pres_square.png"]);
    }

    /**
    * Download Prescription
    *
    * @param $file_name
    *
    * @return mixed
    */

    public function downloading($file_name)
    {
        $email = auth('client')->user()->email;
        $pathToFile = base_path() . '/public/images/prescription/' . $email . '/' . $file_name;

        return Response()->download($pathToFile);
    }
}
