<?php

namespace App\Http\Controllers\V1\Client;

use App\PrescriptionResult;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrescriptionResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PrescriptionResult  $prescriptionResult
     * @return \Illuminate\Http\Response
     */
    public function show(PrescriptionResult $prescriptionResult)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PrescriptionResult  $prescriptionResult
     * @return \Illuminate\Http\Response
     */
    public function edit(PrescriptionResult $prescriptionResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PrescriptionResult  $prescriptionResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrescriptionResult $prescriptionResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PrescriptionResult  $prescriptionResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrescriptionResult $prescriptionResult)
    {
        //
    }
}
