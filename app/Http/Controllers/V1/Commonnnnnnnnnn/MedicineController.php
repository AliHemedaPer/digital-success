<?php
namespace App\Http\Controllers\V1\Common;

use App\Models\Common\MedicineCategory;
use App\Models\Common\MedicineMaterial;
use App\Models\Client\Cart;
use App\Models\Client\Order;
use App\Models\Client\OrderDetail;
use App\Models\Client\OrderToPharmacy;
use App\Models\Common\OrderWallet;
use App\Models\Client\ClientAdress;

use App\Models\Common\Medicine;
use App\Models\Common\Pharmacy;
use App\Models\Common\MedicineUse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App;
use Auth;
use DB;
use Carbon;

//define('CACHE_PARAM_MEDICINE', 'medicines');

class MedicineController extends Controller
{



    /**
     * Get ORders
     *     * @return mixed
     */
    public function getMyOrder()
    {
        if (!Auth::check()) {
            return Redirect::to('/');
        }
        $email = $request->session()->get('user_id');
        $path = URL . '/public/images/prescription/' . $email . '/';
        $user_id = Auth::user()->id;
        $invoices = Invoice::where('user_id', '=', $user_id)->where('shipping_status', '=', ShippingStatus::SHIPPED())->get();


        return View::make('/users/my_order', ['invoices' => $invoices , 'email' => $email , 'default_img' => url() . "/assets/images/no_pres_square.png"]);

        // return View::make('/users/my_order');
    }

    /**
     * Create Prescription List
     */
    public function createPrescriptionList($invoice)
    {
        $medicines = Medicine::medicines();
        $items = [];
        foreach ($invoice->cartList() as $cart) {
            $items[] = ['id' => $cart->id ,
                'item_id' => $cart->medicine ,
                'item_code' => $medicines[$cart->medicine]['item_code'] ,
                'item_name_en' => $medicines[$cart->medicine]['item_name_en'] ,
                'item_name_ar' => $medicines[$cart->medicine]['item_name_ar'] ,
                'price' => $cart->price ,
                'discount_percent' => $cart->discount_percentage ,
                'discount' => $cart->discount ,
                'quantity' => $cart->quantity ,
                'total_price' => $cart->total_price
            ];
        }
        $details = [
            'id' => $invoice->id ,
            'invoice' => $invoice->invoice ,
            'sub_total' => $invoice->sub_total ,
            'discount' => $invoice->discount ,
            'tax' => $invoice->tax ,
            'shipping' => $invoice->shipping ,
            'total' => $invoice->total ,
            'created_on' => $invoice->created_at ,
            'cart' => $items ,
            'shipping_status' => $invoice->shipping_status ,
        ];

        return $details;
    }



    /**
     * Get the paid prescription by logined user
     *
     * @return mixed
     */
    public function getPaidPrescription()
    {
        if (!Auth::check()) {
            return Redirect::to('/');
        }
        // Prescriptions
        $email = $request->session()->get('user_id');
        $path = URL . '/public/images/prescription/' . $email . '/';
        $user_id = Auth::user()->id;
        $invoices = Invoice::where('user_id', '=', $user_id)->where('status_id', '=', InvoiceStatus::PAID())->whereIn('shipping_status', [0 , ShippingStatus::NOTSHIPPED()])->get();

        return View::make('/users/paid_prescription', ['invoices' => $invoices , 'email' => $email , 'cat' => 0 , 'default_img' => url() . "/assets/images/no_pres_square.png"]);
    }

    /**
     * Get Prescription Thumb
     *
     * @return mixed
     */
    public function anyGetPrescriptionThumb()
    {
        try {
            if (!Auth::check()) {
                throw new Exception('You are not authorized to access this content', 401);
            }

            $email = Auth::user()->email;

            if (is_null($email)) {
                throw new Exception('Email is not available', 400);
            }

            $path = url() . '/public/images/prescription/' . $email . '/';

            $user_id = User::where('email', '=', $email)->first()->id;

            if (empty($user_id) || is_null($user_id)) {
                throw new Exception('User not available', 404);
            }

            $prescriptions = Prescription::where('user_id', '=', $user_id)->where('is_delete', '=', 0)->orderBy('id', 'desc')->get();

            if (count($prescriptions) == 0) {
                throw new Exception('prescriptions not available', 404);
            }

            $responses = [];
            $medicines = Medicine::medicines();
            $default_img = url() . "/assets/images/no_pres_square.png";
            foreach ($prescriptions as $prescription) {
                //$prescriptionLink[$i++]=array('link'=>$presLink->path.'_thumb','status_pres'=>$presLink->status);
                $filename = $prescription->path;

                $invoice = $prescription->getInvoice()->first();

                $item_list = $prescription->getCart()->get();

                $items = [];

                foreach ($item_list as $cart) {
                    $items[] = ['id' => $cart->id ,
                        'item_id' => $cart->medicine ,
                        'item_code' => $medicines[$cart->medicine]['item_code'] ,
                        'item_name_en' => $medicines[$cart->medicine]['item_name_en'] ,
                        'item_name_ar' => $medicines[$cart->medicine]['item_name_ar'] ,
                        'price' => $cart->price ,
                        'discount_percent' => $cart->discount_percentage ,
                        'discount' => $cart->discount ,
                        'quantity' => $cart->quantity ,
                        'total_price' => $cart->total_price
                    ];
                }

                if (is_null($invoice)) {
                    continue;
                }

                $data = base_path() . '/public/images/prescription/' . $email . '/' . $filename;
                $status = file_exists(base_path() . '/public/images/prescription/' . $email . '/' . $filename);
                $file = (file_exists(base_path() . '/public/images/prescription/' . $email . '/' . $filename) ? $path . $filename : $default_img);

                $details = [
                    'id' => (is_null($prescription->id)) ? 0 : $prescription->id ,
                    'invoice_id' => (is_null($invoice->id)) ? 0 : $invoice->id ,
                    'invoice' => (is_null($invoice->invoice)) ? 0 : $invoice->invoice ,
                    'sub_total' => (is_null($invoice->sub_total)) ? 0 : $invoice->sub_total ,
                    'discount' => (is_null($invoice->discount)) ? 0 : $invoice->discount ,
                    'tax' => (is_null($invoice->tax)) ? 0 : $invoice->tax ,
                    'shipping' => (is_null($invoice->shipping)) ? 0 : $invoice->shipping ,
                    'total' => (is_null($invoice->total)) ? 0 : $invoice->total ,
                    'created_on' => (is_null($invoice->created_at)) ? 0 : date('Y-m-d', strtotime($invoice->created_at)) ,
                    'cart' => $items ,
                    'shipping_status_id' => (is_null($invoice->shipping_status)) ? 0 : $invoice->shipping_status ,
                    'shipping_status' => (is_null($invoice->shipping_status)) ? '' : ShippingStatus::statusName($invoice->shipping_status) ,
                    'pres_status' => PrescriptionStatus::statusName($prescription->status) ,
                    'pres_status_id' => $prescription->status ,
                    'invoice_status_id' => is_null($invoice->status_id) ? 0 : $invoice->status_id ,
                    'invoice_status' => is_null($invoice->status_id) ? '' : InvoiceStatus::statusName($invoice->status_id) ,
                    'path' => empty($filename) ? $default_img : (file_exists(base_path() . '/public/images/prescription/' . $email . '/' . $filename) ? $path . $filename : $default_img) ,
                ];
                $responses[] = $details;

                $payment_mode = Setting::param('payment', 'mode')['value'];


                //              $link_url = $payment_mode->value ==
                $link_url = "";
                if ($payment_mode == PaymentGateway::PAYU_INDIA()) {
                    $link_url = URL::to('medicine/make-payment/');
                } elseif ($payment_mode == PaymentGateway::PAYPAL()) {
                    $link_url = URL::to('medicine/make-paypal-payment/');
                }
            }

            return Response::json(['status' => 'SUCCESS' , 'msg' => 'Prescriptions Obtained' , 'data' => ['prescriptions' => $responses , 'payment_url' => $link_url , 'currency' => Setting::param('site', 'currency')['value'] , 'curr_position' => Setting::param('site', 'curr_position')['value']]]);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response::make(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }


    /**
     * Load Medicine List
     *
     * @return mixed
     */
    public function anyLoadMedicine()
    {
        header("Access-Control-Allow-Origin: *");
        $medicineName = Input::get('medicine', '');
        $medicine = Medicine::where('item_name_en', 'LIKE', $medicineName . '%')->take(4)->get();
        $i = 0;
        if ($medicine->count() > 0) {
            foreach ($medicine as $med) {
                $medicineNameArray[$i] = ["id" => $i + 1 , "name" => $med->item_name_en , 'mrp' => substr($med->mrp, 0, 4) , 'exp' => $med->expdt , 'item_code' => $med->item_code];
                $i++;
            }
            $result = [['result' => ['status' => 'sucess' , 'msg' => $medicineNameArray]]];
        } else {
            $result = [['result' => ['status' => 'failure']]];
        }

        return Response::json($result);
    }

    public function LoadMedicineWeb(Request $request, $isWeb = 0)
    {
        header("Access-Control-Allow-Origin: *");
        $key = $request->term ?? '';

        $medicines =[];
        if (!empty($key)) {
            $medicines = Medicine::medicines();
            $medicines = array_filter($medicines, function ($medicine) use ($key) {
                $medTemp = $this->stringClean($medicine['item_name_en']);
                $medTemp2 = $this->stringClean($medicine['item_name_ar']);
                $keyTemp = $this->stringClean($key);
                if ((
                    strpos(strtolower($medicine['item_name_en']), strtolower($key)) !== false
                        || strpos(strtolower($medTemp), strtolower($key)) !== false
                        || strpos(strtolower($medTemp), strtolower($keyTemp)) !== false
                        || strpos(strtolower($medTemp2), strtolower($key)) !== false
                        || strpos(strtolower($medTemp2), strtolower($keyTemp)) !== false
                    )
                ) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        if ($isWeb) {
            $json = [];
            foreach ($medicines as $data) {
                $json[] = [
                    'value' => $data['item_name_en'] .'/' . $data['item_name_ar'],
                    'label' => $data['item_name_en'] .'/' . $data['item_name_ar'],
                    'item_code' => $data['item_code'] ,
                ];
            }

            return Response()->json($json);
        } else {
            $medicines = array_slice($medicines, 0, 4);
            if (empty($medicines)) {
                $result = ['status' => 'FAILURE' , 'msg' => 'No Medicines Found' , 'data' => []];
                return Response()->json($result);
            }

            $json = [];
            foreach ($medicines as $data) {
                $json[] = [
                    'value' => $data['item_name_en'] .'/' . $data['item_name_ar'],
                    'label' => $data['item_name_en'] .'/' . $data['item_name_ar'],
                    'item_code' => $data['item_code'] ,
                ];
            }

            $result = ['status' => 'SUCCESS' , 'msg' => 'Search Results' , 'data' => $json];
            return Response()->json($result);
        }
    }

    public function SearchMedicineWeb(Request $request, $key, $is_api = 0)
    {
        $key = $request->key;
        $medicines =[];
        if (!empty($key)) {
        $medicines = Medicine::leftJoin('medicine_to_materials as Medmaterials', 'medicines.id', '=', 'Medmaterials.medicine_id')
                ->leftJoin('medicine_to_uses as Meduses', 'Meduses.medicine_id', '=', 'medicines.id')
                ->leftJoin('medicine_uses as uses', 'uses.id', '=', 'Meduses.use_id')
                ->where("uses.name_en", "like", "%$key%")
                ->Orwhere("uses.name_ar", "like", "%$key%")
                ->Orwhere("medicines.item_name_en", "like", "%$key%")
                ->Orwhere("medicines.item_name_ar", "like", "%$key%")
                ->select('medicines.*')
                ->distinct('id')
                ->paginate(9);
        }
            
        if($is_api){
            //$medicines = $medicines->toArray();
            if (empty($medicines)) {
                $result = ['status' => 'FAILURE' , 'msg' => 'No Medicines Found' , 'data' => []];
                return Response()->json($result);
            }

            $json = [];
            foreach ($medicines as $data) {
                $json[] = [
                    'id' => $data['id'],
                    'category' => $data['category'],
                    'item_code' => $data['item_code'] ,
                    'item_name_en' => $data['item_name_en'] ,
                    'item_name_ar' => $data['item_name_ar'] ,
                    'selling_price' => $data['selling_price'],
                    'image' => $data['image'] ? url('/images/medicines/'.$data['image']) : ''
                ];
            }

            $result = ['status' => 'SUCCESS' , 'msg' => 'Search Results' , 'data' => $json];
            return Response()->json($result);
        }
        
        return view('client.medicine_search', compact('key', 'medicines'));
    }
    
    public function CategoryMedicine(MedicineCategory $category)
    {
        $medicines = Medicine::leftJoin('medicine_to_materials as Medmaterials', 'medicines.id', '=', 'Medmaterials.medicine_id')
                ->leftJoin('medicine_to_uses as Meduses', 'Meduses.medicine_id', '=', 'medicines.id')
                ->leftJoin('medicine_uses as uses', 'uses.id', '=', 'Meduses.use_id')
                ->where("medicines.category",$category->id)              
                ->select('medicines.*')
                ->distinct('id')
                ->paginate(9);
       
        
        
        return view('client.medicine_category', compact('category', 'medicines'));
    }
    /**
     * Clean String Parameter
     */
    public function stringClean($string)
    {
        return preg_replace('/[-" "`*().]/', '', $string);
    }

    /**
     * Load Alternate Medicines
     *
     * @return mixed
     */
    public function loadSubMedicine(Request $request, Medicine $medicine)
    {
        $typ  = $request->typ ?? 'alter';
        // $med_id    = $request->id ?? 0;
        // $med_material = $request->g ?? 0;
        // $med_price = $request->p ?? 1;

        if (!$medicine->id) {
            return Response()->make(['status' => 'FAILURE' , 'msg' => 'No medicines available'], 404);
        }

        $query = Medicine::where('material', '=', $medicine->material)
                         ->where('id', '!=', $medicine->id);
        if($typ == 'simil')
            $query->where('material_values', '=', $medicine->material_values);
        else if($typ == 'alter')
            $query->where('material_values', '!=', $medicine->material_values);

        $medicines = $query->get();
        //$key = Medicine::medicines($med_id);
        if (!count($medicines)) {
            return Response()->make(['status' => 'FAILURE' , 'msg' => 'No medicines available'], 404);
        }

        // $medicines = array_slice ($medicines , 0 , 5);
        // foreach ($medicines as &$value) {
        //  $value['selling_price'] = $value['selling_price'];
        //  $value['mrp'] = $value['selling_price'];
        // }

        $result = ['status' => 'SUCCESS' , 'msg' => 'Alternatives Found !' , 'data' => ['price' => $medicine->selling_price , 'medicines' => $medicines]];

        return Response()->json($result);
    }

    /**
     * Update Medicine cart list
     *
     * @return mixed
     */

    public function anyUpdateBuyMedicine()
    {
        $updatedRows = 0;
        $deletedRow = 0;
        header("Access-Control-Allow-Origin: *");
        $deleted_length = intval(Input::get('deleted_length', 0));
        if ($deleted_length > 0) {
            $invoice_number = Input::get('invoice_number', '');
            for ($i = 0; $i < $deleted_length; $i++) {
                $toBeDeleted = Input::get('item_code' . $i, null);
                try {
                    $rowTobeDeleted = ItemList::where('invoice_number', '=', $invoice_number)->where('item_code', '=', $toBeDeleted)->first();
                    if ($rowTobeDeleted != null) {
                        $deletedRow = $rowTobeDeleted->delete();
                    }
                } catch (Exception $e) {
                }
            }
        }
        if ($deletedRow > 0) {
            $result = [['result' => ['status' => 'success']]];
        } else {
            $result = [['result' => ['status' => 'none']]];
        }

        return Response::json($result);
    }

    /**
     * Update cart list
     *
     * @return mixed
     */

    public function anyUpdateBuy()
    {
        $updatedRows = 0;
        $deletedRow = 0;
        header("Access-Control-Allow-Origin: *");
        $deleted_length = intval(Input::get('deleted_length', 0));
        if ($deleted_length > 0) {
            $invoice_number = Input::get('invoice_number', '');
            for ($i = 0; $i < $deleted_length; $i++) {
                $toBeDeleted = Input::get('item_code' . $i, null);
                try {
                    $rowTobeDeleted = ItemList::where('invoice_number', '=', $invoice_number)->where('item_code', '=', $toBeDeleted)->first();
                    if ($rowTobeDeleted != null) {
                        $deletedRow = $rowTobeDeleted->delete();
                    }
                } catch (Exception $e) {
                }
            }
        }
        if ($deletedRow > 0) {
            $result = [['result' => ['status' => 'success']]];
        } else {
            $result = [['result' => ['status' => 'none']]];
        }

        return Response::json($result);
    }

    /**
     * Update A New Medicine Request
     *
     * @return mixed
     */

    public function anyAddMedicine()
    {
        $name = Input::get('name', '');
        $oldMed = NewMedicine::where('name', '=', $name)->get();
        if ($oldMed->count() > 0) {
            $newCount = ['count' => $oldMed->first()->count + 1 , 'updated_at' => date('Y-m-d H:i:s')];
            $affectedRows = NewMedicine::where('name', '=', $name)->update($newCount);
            $who = new NewMedicineEmail;
            $who->email = Input::get('email', '');
            $who->request_id = $oldMed->first()->id;
            $who->created_at = date('Y-m-d H:i:s');
            $who->save();
        } else {
            $newMed = new NewMedicine;
            $newMed->name = $name;
            $newMed->count = 1;
            $newMed->created_at = date('Y-m-d H:i:s');
            $newMed->save();
            $who = new NewMedicineEmail;
            $who->email = Input::get('email', '');
            $who->request_id = $newMed->id;
            $who->created_at = date('Y-m-d H:i:s');
            $who->save();
        }
        $result = [['result' => ['status' => 'success']]];

        return Response::json($result);
    }

    /**
     * Get Prescription Image
     *
     * @return mixed
     */

    public function postGetPresImg()
    {
        $pres_id = Input::get('pres_id');
        $u = User::join('prescription', 'prescription.user_id', '=', 'users.id')->where('id', '=', $pres_id)->first();
        $path = url() . '/public/images/prescription/' . $u->email . '/' . $u->path;
        $result = [['result' => ['status' => 'success' , 'link' => $path]]];

        return Response::json($result);
    }

    /**
     * Get Medicine Details Search option
     *
     * @param $serched_medicine
     *
     * @return mixed
     */

    public function getMedicineDetail($searched_medicine)
    {
        $med_info = Medicine::select('*')
            ->where('item_code', '=', $searched_medicine)
            ->get();
        if (count($med_info) > 0) {
            return view('client.medicine_detail')->with(compact('med_info'));
        } else {
            return back()->withErrors(['Sorry no more search results available']);
        }
    }

    /**
     * Get User Cart
     *
     * @return mixed
     */

    public function getMyCart()
    {
        $client_id = auth('client')->id();
        $current_orders = Cart::select(
                                    'client_cart.*',
                                     'medicines.discount',
                                     'medicines.composition_en',
                                     'medicines.composition_ar',
                                     'medicines.manufacturer',
                                     'medicines.item_code',
                                     'medicines.material'
                            )
                            ->leftJoin('medicines', 'client_cart.medicine_id', '=', 'medicines.id')
                            ->where('client_id', '=', $client_id)->get();
        $materials = MedicineMaterial::all()->pluck('name', 'id')->toArray();
        $addresses = ClientAdress::where('client_id', '=', $client_id)->where('is_other', '=', 0)->get();
        
        return View('client.cart')->with(compact('current_orders', 'materials', 'addresses'));
    }
    /*
     * function to get current iems in cart (used in header menu)
     */
    public static function MenuCart()
    {
        $client_id = auth('client')->id();
        $current_orders = Cart::select(
                                    'client_cart.*',
                                     'medicines.discount',
                                     'medicines.composition_en',
                                     'medicines.composition_ar',
                                     'medicines.manufacturer',
                                     'medicines.item_code',
                                     'medicines.material'
                            )
                            ->leftJoin('medicines', 'client_cart.medicine_id', '=', 'medicines.id')
                            ->where('client_id', '=', $client_id)->get();
        
        
        return $current_orders;
    }
    /* 
     * remove from header cart by ajax 
     */
     public function removeFromMenuCart($item_id)
    {
        $status = DB::table('client_cart')->where('id', '=', $item_id)->delete();        
        return $status;
    }
    /*
     * remove item from cart
     * deletes row from 'sessions'  table
     * */

    public function removeFromCart($item_id)
    {
        DB::table('client_cart')->where('id', '=', $item_id)->delete();

        return back()->withErrors(['msg' , 'Item has been removed']);
    }

    /*
     * View item information
     * */

    public function anyViewItemInfo($item_code)
    {
        $item_details = DB::table('medicine')
            ->where('item_code', '=', $item_code)
            ->get();
        $email = $request->session()->get('user_id');
        $current_orders = DB::table('client_cart')
            ->where('client_id', '=', $email)
            ->get();

        return View::make('/users/my_cart', ['current_orders' => $current_orders , 'item_details' => $item_details]);
    }

    /*
     * make user orders
     * stores or updates each orders to sessions table
     * */

    public function AddCart(Request $request, $is_web = 0)
    {
//            if (!$this->isCsrfAccepted()) {
//                return 0;
//            }

        $medicine = $request->medicine;
        $med_quantity = $request->quantity;
        $med_unit = $request->unit;
        //$med_price = $request->price;
        $item_code = $request->item_code;
        $item_id = $request->id;


        // $request->session()->put('medicine' , $medicine);
        // $request->session()->put('med_quantity' , $med_quantity);
        // $request->session()->put('med_mrp' , $med_mrp);
        // $request->session()->put('item_code' , $item_code);
        // $request->session()->put('item_id' , $item_id);

        $client_id = auth('client')->id();
        if (auth('client')->check()) {
            $medicine_exist = DB::table('client_cart')->select('medicine_name')->where('client_id', '=', $client_id)->where('medicine_id', '=', $item_id)->get();
            if (count($medicine_exist) > 0) {
                $increment = DB::table('client_cart')->where('client_id', '=', $client_id)->where('medicine_id', '=', $item_id)->increment('quantity', $med_quantity);
                if ($increment) {
                    // $request->session()->forget ('medicine');
                    // $request->session()->forget ('med_quantity');
                    // $request->session()->forget ('med_mrp');
                    // $request->session()->forget ('item_code');
                    // $request->session()->forget ('item_id');


                    if ($is_web == 1) {
                        return view("cient.cart");
                    } else {
                        return "updated";
                    }
                }
            } else {
                $insert = DB::table('client_cart')->insert([
                                                                'medicine_id' => $item_id ,
                                                                'medicine_name' => $medicine ,
                                                                'quantity' => $med_quantity,
                                                                'client_id' => $client_id ,
                                                                //'price' => $med_price ,
                                                                'unit_id' => $med_unit,
                                                                'item_code' => $item_code
                                                            ]);
                if ($insert) {
                    //return "updated";
                    // $request->session()->forget('medicine');
                    // $request->session()->forget('med_quantity');
                    // $request->session()->forget('med_price');
                    // $request->session()->forget('item_code');
                    // $request->session()->forget('item_id');


                    if ($is_web == 1) {
                        return view("cient.cart");
                    } else {
                        return "inserted";
                    }
                }
            }
        } else {
            return 0;
        }
    }

    /**
     * Update Cart
     */

    public function updateCart(Request $request)
    {
        // if (!$this->isCsrfAccepted ())
        //  return 0;
        // Update Item
        $item_code = $request->item_code;
        $new_val = $request->new_val;
        $column = $request->column;

        $client_id = auth('client')->id();
        $qty_updt = DB::table('client_cart')
            ->where('client_id', '=', $client_id)
            ->where('item_code', '=', $item_code)
            ->update([$column => $new_val]);

        if ($qty_updt) {
            echo 1;
        } else {
            echo 0;
        }
    }

    /**
     * Pharmacy search proccess
     */

    public function pharmcySearch(Request $request)
    {
        $client_id  = auth('client')->id();
        $address_id = $request->address;

        if (auth('client')->user()->status === -1) {
            return Response()->json(['status' => 0, 'msg' => 'permissionerror']);
        }

        if($address_id == -1){
            $oth_dtls   = $request->oth_dtls;
            $latitude  = $request->oth_lat;
            $longitude   = $request->oth_long;

            $other_address = ClientAdress::create([
                'client_id' => auth('client')->user()->id,
                'title' => 'Other Address',
                'address' => $oth_dtls,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'primary' => 0,
                'is_other' => 1
            ]);

            if (!$other_address) {
                return Response()->json(['status' => 0, 'msg' => 'noprimary']);
            }

            $address_id = $other_address->id;

        }else{
            // get user address
            $client_address = ClientAdress::where('id', '=', $address_id)->limit(1)->get();

            if (!count($client_address) || $client_address[0]->client_id != $client_id ) {
                return Response()->json(['status' => 0, 'msg' => 'noprimary']);
            }
            $longitude = $client_address[0]->longitude;
            $latitude = $client_address[0]->latitude;
        }

        

        

        $phIds = [];
        $expand = false;
        if(isset($request->order)){
            $expand = true;
            $order_id = $request->order;
            //Get ordered pharmacies
            $phIds = OrderToPharmacy::select('pharmacy_id')->where('order_id', '=', $request->order)->groupBy('pharmacy_id')->pluck('pharmacy_id')->toArray();
            
            //Get order Details Items
            $cart = OrderDetail::where('order_id', '=', $request->order)->get();
        }else{
            //Get cart Items
            $cart = Cart::where('client_id', '=', $client_id)->get();
        }
        
        if (!count($cart)) {
            return Response()->json(['status' => 0, 'msg' => 'emptycart']);
        }

        $app_setting = DB::table('settings')->select('ph_search_number')->first();
        // get nearst pharmacy
        $pharmaciesQuery = DB::table('pharmacies')->select(DB::raw("*, ( 6367 * acos( cos( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( latitude ) ) ) ) AS distance"))
        ->where('status', '=', 1);
        if($expand && count($phIds)){
            $pharmaciesQuery->whereNotIn('id', $phIds);
        }
        $pharmacies = $pharmaciesQuery->limit($app_setting->ph_search_number)
                                      ->orderBy('distance')
                                      ->get();
        if (!count($pharmacies)) {
            return Response()->json(['status' => 0, 'msg' => 'nopharmacy']);
        }

        //print_r($pharmacies);
        //exit;
        //Create the order if not expand
        if(!$expand){
            $order = Order::create([
                'client_id'   => $client_id,
                'client_address_id' => $address_id,
                'total_price' => 0,
                'status' => 'unverified'
            ]);
            if (!$order){
                return Response()->json(['status' => 0, 'msg' => 'ordererror']);
            }
            $order_id = $order->id;
        }

        $loop = 0;
        foreach ($pharmacies as $pharmacy_id) {
            $loop++;
            //create wallet for each pharmacy
            OrderWallet::insert([
                        'order_id'   => $order_id,
                        'pharmacy_id' => $pharmacy_id->id,
                        'total_cost' => 0,
                        'mdcn_cost' => 0,
                        'deliv_cost' => 0,
                        'status' => 'pending',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

            foreach ($cart as $item) {
                //save cart items once
                if ($loop == 1 && !$expand) {
                    OrderDetail::insert([
                        'order_id'   => $order_id,
                        'medicine_id' => $item->medicine_id,
                        'unit_id' => $item->unit_id, 
                        'quantity' => $item->quantity,
                        'price' => $item->units->price,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }

                //Create cart items for each pharmacy
                OrderToPharmacy::insert([
                    'order_id'   => $order_id,
                    'medicine_id' => $item->medicine_id,
                    'pharmacy_id' => $pharmacy_id->id,
                    'unit_id' => $item->unit_id, 
                    'quantity' => $item->quantity,
                    'price' => $item->units->price,
                    'available' => 0,
                    'limited' => 0,
                    'limited_with_all' => 0,
                    'ph_status' => 'pending',
                    'cl_status' => 'pending',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
        }

        if(!$expand){
            DB::table('client_cart')->where('client_id' , '=' , $client_id)->delete();
        }
        
        return Response()->json(['status' => 1, 'order_id' => $order_id]);
    }

    public function orders($status)
    {
        $client_id = auth('client')->id();

        $query = Order::where('client_id', '=', $client_id);
        if ($status !== 'all') {
            $query->where('status', '=', $status);
        }

        $orders = $query->latest()->get();

        return View('client.orders')->with(compact('orders', 'status'));
    }

    public function orderPharmaciesList(Request $request, Order $order)
    {
        $client_id = auth('client')->id();

        $from = $order->created_at;
        $to = Carbon::now();
        $leftMin = $from->diffInMinutes($to);
        $leftSec = $from->diffInSeconds($to);

        $client_address = ClientAdress::where('id', '=', $order->client_address_id)
                                      ->limit(1)->get();

        if ($order->client_id != $client_id || !count($client_address)) {
            return back();
        }

        $cl_lat = $client_address[0]->latitude;
        $cl_lon = $client_address[0]->longitude;

        $orderDetails = OrderToPharmacy::with('units')
                                       ->select(
                                                'order_to_pharmacies.id as item_id',
                                                'order_to_pharmacies.*',
                                                'pharmacies.*'
                                                )
                                        ->where('order_id', '=', $order->id)
                                        ->leftJoin('pharmacies', 'pharmacies.id', '=', 'order_to_pharmacies.pharmacy_id')
                                        ->orderBy('pharmacy_id', 'desc')->get();

        $pharmacies = $orderDetails->groupBy('name');

        return View('client.order-pharmacies')->with(compact('pharmacies', 'order', 'cl_lat', 'cl_lon', 'leftSec'));
    }

    public function orderPharmaciesBuy(Order $order, Pharmacy $pharmacy)
    {
        $client_id = auth('client')->id();
        
        $client_address = ClientAdress::where('id', '=', $order->client_address_id)
                                      ->limit(1)->get();
        if ($order->client_id != $client_id || !count($client_address)) {
            return back();
        }
        $cl_lat = $client_address[0]->latitude;
        $cl_lon = $client_address[0]->longitude;

        $orderDetails = OrderToPharmacy::where('order_id', '=', $order->id)
                                        ->where('pharmacy_id', '=', $pharmacy->id)
                                        ->where('available', '=', 1)
                                        ->get();

        return View('client.order-buy')->with(compact('orderDetails', 'order', 'pharmacy', 'cl_lat', 'cl_lon'));
    }

    public function orderBuyProccess(Request $request, Order $order, Pharmacy $pharmacy)
    {
        $client_id = auth('client')->id();
        $order_id  = $order->id;

        $client_address = ClientAdress::where('id', '=', $order->client_address_id)
                                      ->limit(1)->get();
        if ($order->client_id != $client_id || !count($client_address)) {
            session()->flash('No Primary address detected!', 1);
            return back();
        }
        $cl_lat = $client_address[0]->latitude;
        $cl_lon = $client_address[0]->longitude;

        if ($order->client_id != $client_id) {
            session()->flash('This is not your Order!', 1);
            return back();
        }

        $reqMedicines = $request->medicines;
        if (!count($reqMedicines)) {
            session()->flash('noitems', 1);
            return back();
        }  

        $stp1 = OrderToPharmacy::whereIn('id', $reqMedicines)
                        ->where('order_id', '=', $order_id)
                        ->update(['ph_status' => 'buy', 'cl_status' => 'buy']);
        if(!$stp1){
            session()->flash('Proccess Error, Please Try later or contact Customer Support!', 1);
            return back();
        }

        OrderToPharmacy::where('pharmacy_id', '=', $pharmacy->id)
                       ->where('order_id', '=', $order_id)
                       ->whereNotIn('id', $reqMedicines)
                       ->update(['ph_status' => 'replied', 'cl_status' => null]);

        $medicines = OrderToPharmacy::whereIn('id', $reqMedicines)
                        ->where('order_id', '=', $order_id)
                        ->get()->toArray();

        //Calculate cost
        $deliverCost = $pharmacy->orderToPharmacy[0]->latlongdistance($cl_lat, $cl_lon) * 1;
        $total = 0;
        foreach ($medicines as $key => $value) {
            $total += $value['price'] * $value['quantity'];
        }

        /////////////////
        OrderWallet::where('pharmacy_id', '=', $pharmacy->id)
                    ->update(['total_cost' => $total + $deliverCost,
                              'mdcn_cost' =>  $total,
                              'deliv_cost' => $deliverCost,
                              'status' => 'compeleted',
                              'payment_type' => 'cashe_on_delivery']);

        $order->update(['status' => 'buy']);

        return View('client.order-buy-success');
    }

    public function ordersRemove(Order $order)
    {
        $client_id = auth('client')->id();
        if ($client_id == $order->client_id) {
            $order->delete();
            return Response()->make(['status' => 'success']);
        }
        return Response()->make(['status' => 'FAILURE' , 'msg' => 'Error'], 404);
    }


    public function orderResult(Request $request)
    {
        return Response()->json(['status' => 1]);
    }

    public function refund(Request $request, OrderToPharmacy $id)
    {
        $reason = $request->reason;
        if(!$reason)
            return Response()->make(['status' => 0 , 'msg' => 'Empty Reason'], 404);

        $id->update([ 'refund_status' => 1, 'refund_reason' => $request->reason ]);
        return Response()->make(['status' => 1]);
    }

    /**
     * @param $invoice
     *
     * @return mixed
     * render the view of payment with payment details
     */

    public function anyMakePayment($invoice, $isMobile = 0)
    {
        // If User Authenticated
        if (!Auth::check()) {
            return Redirect::to('/');
        }
        // Get Invoice
        $invoiceDetails = Invoice::find($invoice);
        // If Invoice Is Not Present
        if (is_null($invoice)) {
            return Redirect::to('/paid-prescription');
        }
        $data = [];
        $email = $request->session()->get('user_id');
        $user = Auth::user();
        $type = $user->user_type_id;
        if ($type == UserType::CUSTOMER()) {
            $user_info = Customer::find($user->user_id);
            $phone = $user_info->phone;
            $fname = $user_info->first_name;
            $lname = $user_info->last_name;
            $address = $user_info->address;
        } elseif ($type == UserType::MEDICAL_PROFESSIONAL()) {
            $user_info = MedicalProfessional::find($user->user_id);
            $phone = $user_info->prof_phone;
            $fname = $user_info->prof_first_name;
            $lname = $user_info->prof_last_name;
            $address = $user_info->prof_address;
        }
        $data = [];
        $item_name = "";
        $i = 0;
        foreach ($invoiceDetails->cartList() as $cart) {
            $item_name .= Medicine::medicines($cart->medicine)['item_name_en'];
            $item_name .= " ,";
        }
        $total = $invoiceDetails->total;
        $data['amount'] = $total;
        $data['email'] = $email;
        $data['phone'] = $phone;
        $data['firstname'] = $fname;
        $data['lname'] = $lname;
        $data['address'] = $address;
        $data['invoice'] = $invoiceDetails->invoice;
        $data['id'] = $invoice;
        $data['productinfo'] = $item_name;

        if ($isMobile) {
            return View::make('/users/mobile_payment', ['posted' => $data]);
        } else {
            return View::make('/users/payment', ['posted' => $data]);
        }
    }

    /**
     * @param $invoice
     *
     * @return mixed
     * render the view of payment with payment details
     */

    public function anyMakePaypalPayment($invoice, $isMobile = 0)
    {
        // If User Authenticated
        if (!Auth::check()) {
            return Redirect::to('/');
        }
        // Get Invoice
        $invoiceDetails = Invoice::find($invoice);
        // If Invoice Is Not Present
        if (is_null($invoice)) {
            return Redirect::to('/paid-prescription');
        }
        $data = [];
        $email = $request->session()->get('user_id');
        $user = Auth::user();
        $type = $user->user_type_id;
        if ($type == UserType::CUSTOMER()) {
            $user_info = Customer::find($user->user_id);
            $phone = $user_info->phone;
            $fname = $user_info->first_name;
            $lname = $user_info->last_name;
            $address = $user_info->address;
        } elseif ($type == UserType::MEDICAL_PROFESSIONAL()) {
            $user_info = MedicalProfessional::find($user->user_id);
            $phone = $user_info->prof_phone;
            $fname = $user_info->prof_first_name;
            $lname = $user_info->prof_last_name;
            $address = $user_info->prof_address;
        }
        $data = [];
        $item_name = "";
        $i = 0;
        foreach ($invoiceDetails->cartList() as $cart) {
            $item_name .= Medicine::medicines($cart->medicine)['item_name_en'];
            $item_name .= " ,";
        }
        $total = $invoiceDetails->total;
        $data['amount'] = $total;
        $data['email'] = $email;
        $data['phone'] = $phone;
        $data['firstname'] = $fname;
        $data['lname'] = $lname;
        $data['address'] = $address;
        $data['invoice'] = $invoiceDetails->invoice;
        $data['id'] = $invoice;
        $data['productinfo'] = $item_name;

        if ($isMobile) {
            return View::make('/users/mobile_paypal_payment', ['posted' => $data]);
        } else {
            return View::make('/users/paypal_payment', ['posted' => $data]);
        }
    }

    /**
     * URL for success payment
     *
     * @param $invoice
     */

    public function anyPaySuccess($invoice)
    {
        $transaction_id = Input::get('payuMoneyId', '');             // Save Return Transaction Id of Payment Gateway
        // Update Invoice
        $invoice = Invoice::find($invoice);
        $invoice->status_id = InvoiceStatus::PAID();
        $invoice->payment_status = PayStatus::SUCCESS();
        $invoice->transaction_id = $transaction_id;
        $invoice->updated_at = date('Y-m-d H:i:s');
        $invoice->updated_by = Auth::user()->id;
        $invoice->save();
        // User
        $user_detail = $invoice->getUser;
        $type = $user_detail->user_type_id;
        // Send Paid Mail
        if ($type == UserType::CUSTOMER()) {
            $user = Customer::select('mail', 'first_name')->find($user_detail->user_id);
            $user_email = $user->mail;
            $user_name = $user->first_name;
        } elseif ($type == UserType::MEDICAL_PROFESSIONAL()) {
            $user = MedicalProfessional::select('prof_mail as mail', 'prof_first_name as first_name')->find($user_detail->user_id);
            $user_email = $user->mail;
            $user_name = $user->first_name;
        }
        Mail::send('emails.paid', ['name' => $user_name], function ($message) use ($user_email) {
            $message->to($user_email)->subject('Your payment received at ' . Setting::param('site', 'app_name')['value']);
        });

        return Redirect::to('/payment/success');
    }

    /**
     * URL for success payment from paypal
     *
     * @return mixed
     */

    public function anyPaypalSuccess()
    {
        session_start();
        session_destroy();
        $invoice = Input::get('pay_id', '');
        $transaction_id = Input::get('transaction_id', '');
        if ($transaction_id != abs(crc32($invoice))) {
            session_start();
            session_destroy();

            return View::make('/users/payment_failed');
        }
        $invoice = Invoice::where('invoice', '=', $invoice)->first();
        $invoice->status_id = InvoiceStatus::PAID();
        $invoice->payment_status = PayStatus::SUCCESS();
        $invoice->transaction_id = $transaction_id;
        $invoice->updated_at = date('Y-m-d H:i:s');
        $invoice->updated_by = Auth::user()->id;
        $invoice->save();
        // User
        $user_detail = $invoice->getUser;
        $type = $user_detail->user_type_id;
        // Send Paid Mail
        if ($type == UserType::CUSTOMER()) {
            $user = Customer::select('mail', 'first_name')->find($user_detail->user_id);
            $user_email = $user->mail;
            $user_name = $user->first_name;
        } elseif ($type == UserType::MEDICAL_PROFESSIONAL()) {
            $user = MedicalProfessional::select('prof_mail as mail', 'prof_first_name as first_name')->find($user_detail->user_id);
            $user_email = $user->mail;
            $user_name = $user->first_name;
        }
        Mail::send('emails.paid', ['name' => $user_name], function ($message) use ($user_email) {
            $message->to($user_email)->subject('Your payment received at ' . Setting::param('site', 'app_name')['value']);
        });

        //      return View::make ('/users/payment_success');
        return Redirect::to('/payment/success');
    }

    /**
     * make an invoice paid by admin
     *
     * @param $invoice
     */

    public function anyAdminPaySuccess($invoice)
    {
        // Update Invoice
        $invoice = Invoice::find($invoice);
        $invoice->status_id = InvoiceStatus::PAID();
        $invoice->payment_status = PayStatus::SUCCESS();
        $invoice->updated_at = date('Y-m-d H:i:s');
        $invoice->updated_by = Auth::user()->id;
        $invoice->save();
        // User
        $user_detail = $invoice->getUser;
        $type = $user_detail->user_type_id;
        // Send Paid Mail
        if ($type == UserType::CUSTOMER()) {
            $user = Customer::select('mail', 'first_name')->find($user_detail->user_id);
            $user_email = $user->mail;
            $user_name = $user->first_name;
        } elseif ($type == UserType::MEDICAL_PROFESSIONAL()) {
            $user = MedicalProfessional::select('prof_mail as mail', 'prof_first_name as first_name')->find($user_detail->user_id);
            $user_email = $user->mail;
            $user_name = $user->first_name;
        }
        Mail::send('emails.paid', ['name' => $user_name], function ($message) use ($user_email) {
            $message->to($user_email)->subject('Your payment received at ' . Setting::param('site', 'app_name')['value']);
        });

        return Redirect::to('/admin/load-active-prescription');
    }

    /**
     * URL for failed payment
     *
     * @param $invoice
     */

    public function anyPayFail($invoice)
    {
        return Redirect::to('/payment/failure');
    }

    /**
     * URL for Failed payment
     */

    public function anyPaypalFail()
    {
        return Redirect::to('/payment/failure');
    }

    /*
     * View item information
     * */

    public function getMedicineData(Request $request)
    {
        try {
            $mid = $request->id ?? 0;

            if (empty($mid)) {
                throw new Exception('empty medicine id passed !', 400);
            }

            $medicine = Medicine::medicines($mid);

            if (empty($medicine)) {
                throw new Exception('medicine not found !', 404);
            }

            return Response()->json(['status' => 'SUCCESS' , 'msg' => 'Medicine data obtained !' , 'data' => $medicine ]);
        } catch (Exception $e) {
            $message = $this->catchException($e);
            return Response()->json(['status' => 'FAILURE' , 'msg' => $message['msg']], $message['code']);
        }
    }

    /**
     * Name search in medicine list
     *
     * @return mixed
     */

    public function getMedicineListFromName()
    {
        $name = Input::get('name');
        $order = Input::get('ord', 'ASC');
        if ($name != "") {
            $medicines = Medicine::select('id', 'item_name_en as name_en', 'item_name_ar as name_ar', 'batch_no', 'manufacturer as mfg', 'material', 'expiry as exp', 'item_code', 'selling_price as mrp', 'composition_en', 'composition_ar', 'is_pres_required')->where('item_name_en', 'LIKE', $name . "%")->orderBy('composition_en', $order)->where('is_delete', '=', 0)->paginate(30);
        } else {
            $medicines = Medicine::select('id', 'item_name_en as name_en', 'item_name_ar as name_ar' , 'batch_no', 'manufacturer as mfg', 'material', 'expiry as exp', 'item_code', 'selling_price as mrp', 'composition_en', 'composition_ar', 'is_pres_required')->where('item_name_ar', 'LIKE', $name . "%")->orderBy('composition_en', $order)->where('is_delete', '=', 0)->paginate(30);
        }

        return Response::json(['medicines' => $medicines->getCollection() , 'link' => $medicines->links()->render()]);
    }

    /**
     * Add new medicine for web
     *
     * @return mixed
     */

    public function AddNewMedicine(Request $request)
    {
        $name = $request->name;
        $user_id = 0;
        $email = 'Not Available';
        if (Auth::check()) {
            $email = Auth::user()->email;
            $user_id = Auth::user()->id;
        }
        $oldMed = NewMedicine::where('name', '=', $name)->get();
        if ($oldMed->count() > 0) {
            $newCount = ['count' => $oldMed->first()->count + 1 , 'updated_at' => date("Y-m-d H:i:s")];
            $affectedRows = NewMedicine::where('name', '=', $name)->update($newCount);
            $who = new NewMedicineEmail;
            $who->email = $email;
            $who->request_id = $oldMed->first()->id;
            $who->user_id = $user_id;
            $who->created_at = date('Y-m-d H:i:s');
            $status = $who->save();
        } else {
            $newMed = new NewMedicine;
            $newMed->name = $name;
            $newMed->count = 1;
            $newMed->created_at = date("Y-m-d H:i:s");
            $newMed->save();
            $who = new NewMedicineEmail;
            $who->email = $email;
            $who->request_id = $newMed->id;
            $who->user_id = $user_id;
            $who->created_at = date("Y-m-d H:i:s");
            $status = $who->save();
        }

        return Response()->json(['status' => $status]);
    }

    /**
     * Upload Bulk Medicine List
     */

    public function postUpload()
    {
        try {
            if (!$this->isCsrfAccepted()) {
                throw new Exception('FORBIDDEN', 403);
            }
            if (!Input::hasFile('file')) {
                throw new Exception('BAD REQUEST', 400);
            }
            $file = Input::file('file');
            $extension = strtolower($file->getClientOriginalExtension());
            if (!in_array($extension, ['xls' , 'xlsx'])) {
                throw new Exception('Invalid File Uploaded ! Please upload either xls or xlsx file', 400);
            }
            Excel::selectSheetsByIndex(0)->load($file, function ($reader) {
                // Getting all results
                $content = $reader->get();
                $results = [];
                $aAllMedcines = Medicine::select('item_name_en')->get()->toArray();
                $available_medicines = array_column($aAllMedcines, 'item_name_en');
                $availableMed = array_map('trim', $available_medicines);

                $iLoggedUserId = Auth::user()->id;
                $curDate = date('Y-m-d H:i:s');
                $i=0;
                foreach ($content as $result) {
                    $itemName = ((isset($result->item_name_en) && !empty($result->item_name_en)) ? trim($result->item_name_en) : '');
                    if (!$itemName || in_array(trim($result->item_name_en), $availableMed)) {
                        continue;
                    }

                    $results = ['item_code' => ((isset($result->item_code) && !empty($result->item_code)) ? $result->item_code : '') ,
                        'item_name_en' => $itemName ,
                        'item_name_ar' => $itemName ,
                        'batch_no' => ((isset($result->batch_no) && !empty($result->batch_no)) ? $result->batch_no : '') ,
                        'quantity' => ((isset($result->quantity) && !empty($result->quantity)) ? $result->quantity : 0) ,
                        'cost_price' => ((isset($result->cost_price) && !empty($result->cost_price)) ? $result->cost_price : 0.00) ,
                        'purchase_price' => ((isset($result->purchase_price) && !empty($result->purchase_price)) ? $result->purchase_price : 0.00) ,
                        'rack_number' => ((isset($result->rack) && !empty($result->rack)) ? $result->rack : '') ,
                        'selling_price' => ((isset($result->mrp) && !empty($result->mrp)) ? $result->mrp : 0.00) ,
                        'expiry' => ((isset($result->expiry) && !empty($result->expiry)) ? $result->expiry : '') ,
                        'tax' => ((isset($result->tax) && !empty($result->tax)) ? $result->tax : 0.00) ,
                        'composition_en' => ((isset($result->composition_en) && !empty($result->composition_en)) ? $result->composition_en : '') ,
                        'composition_ar' => ((isset($result->composition_ar) && !empty($result->composition_ar)) ? $result->composition_ar : '') ,
                        'discount' => ((isset($result->discount) && !empty($result->discount)) ? $result->discount : 0.00) ,
                        'manufacturer' => ((isset($result->manufactured_by) && !empty($result->manufactured_by)) ? $result->manufactured_by : '') ,
                        'marketed_by' => ((isset($result->marketed_by) && !empty($result->marketed_by)) ? $result->marketed_by : '') ,
                        'material' => ((isset($result->material) && !empty($result->material)) ? $result->material : '') ,
                        'created_at' => $curDate ,
                        'created_by' => $iLoggedUserId ,
                    ];

                    Medicine::insert($results);
                    $availableMed[] = $itemName;
                }
            });

            return Response::json('success', 200);
        } catch (Exception $e) {
            return Response::json(['msg' => $e->getMessage()], $e->getCode());
        }
    }
}
