<?php
namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Auth\AuthenticationException;

/**
 * Class CheckRoleMiddleware
 * @package App\Http\Middleware
 */
class CheckRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth('admin')->user();

        if ($user) {
            return $next($request);
        }
        throw new AuthenticationException('Unauthenticated.', ['admin']);
        return response([
            'error' => [
                'code' => 'INSUFFICIENT_ROLE',
                'description' => 'You are not authorized to access this resource.'
            ]
        ], 401);
    }
}
