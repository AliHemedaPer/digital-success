<?php 

namespace App\Http\ViewComposers;

use App;
use DB;
use Illuminate\Contracts\View\View;

class SettingsComposer
{
    public function compose(View $view) {
    	$locale      = App::getLocale();
    	$settings    = DB::table('settings')->get();
    	$categories  = DB::table('categories')->get();
    	$services    = DB::table('posts')->where('post_type', 'service')->get();

        $view->with('appSettings', $settings[0]);
        $view->with('share_locale', $locale);
        $view->with('appCategories', $categories);
        $view->with('appServices', $services);
    }
}