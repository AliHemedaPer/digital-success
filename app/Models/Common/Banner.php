<?php

namespace App\Models\Common;

use App;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{    
     protected $guarded = [];

    public function getTitleAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'title_'.$local };
    }

    public function getShortDescAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'short_desc_'.$local };
    }
}
