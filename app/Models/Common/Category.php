<?php

namespace App\Models\Common;

use App;
use App\Models\Common\Post;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $guarded = [];
	
    public function posts()
    {
        return $this->hasMany(Post::class, 'category');
    }

    public function getNameAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'name_'.$local };
    }

    public function getContentAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'content_'.$local };
    }
}
