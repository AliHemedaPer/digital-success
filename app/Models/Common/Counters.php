<?php

namespace App\Models\Common;

use App;
use Illuminate\Database\Eloquent\Model;

class Counters extends Model
{
    protected $guarded = [];

    public function getTitleAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'title_'.$local };
    }
}
