<?php

namespace App\Models\Common;

use App;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    protected $guarded = [];

    public function postCategory()
    {
        return $this->belongsTo(Category::class, 'category', 'id');
    }

    public function getTitleAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'title_'.$local };
    }

    public function getContentAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'content_'.$local };
    }

    public function getShortDescAttribute()
    {
    	$local = App::getLocale();
        return $this->{ 'short_desc_'.$local };
    }
}
