<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
          view()->composer('*',function($view) {
            $view->with('name', 'name_'.\App::getLocale());
            $view->with('composition', 'composition_'.\App::getLocale());
            $view->with('item_name', 'item_name_'.\App::getLocale());
           
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
