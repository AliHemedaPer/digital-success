<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category')->nullable()->default(0);
            $table->string('title_en', 255);
            $table->string('title_ar', 255);
            $table->text('short_desc_en')->nullable();
            $table->text('short_desc_ar')->nullable();
            $table->longtext('content_en');
            $table->longtext('content_ar');
            $table->string('video', 255)->nullable();
            $table->string('post', 10);
            $table->string('image', 255)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
