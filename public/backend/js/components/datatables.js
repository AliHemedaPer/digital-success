/* ------------------------------------------------------------------------------
 *
 *  # Advanced datatables
 *
 *  Demo JS code for datatable_advanced.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------
var currentScript = document.currentScript
var DatatableAdvanced = function() {


    //
    // Setup module components
    //

    // Basic Datatable examples
    var _componentDatatableAdvanced = function(target, columnsNum, valueNum) {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                }
            }
        });


        // Basic initialization
        var $oTable = $(target === '*' ? 'table' : target).DataTable({
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [/*{
                    extend: 'copy'
                }, {
                    extend: 'csv'
                }, {
                    extend: 'excel'
                }, {
                    extend: 'pdf'
                }, {
                    extend: 'print'
                }*/]
            }
        });

        // if(columnsNum && valueNum && !parseInt(valueNum)) {
        //     console.log($oTable)
            // $oTable.fnAddData(Array.from({ length: parseInt(columnsNum) }, (_, idx) => `${++idx}`))
        // }
    };

    var getParams = function(text) {
        var vars = {},
            parts = text.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
                vars[key] = value;
            });
        return vars;
    };

    return {
        init: function() {
            if (currentScript) {
                var params = getParams(currentScript.src);
                _componentDatatableAdvanced(params.target || '*', params.columnsNum, params.valueNum);
            }
        }
    }
}();


// Initialize module
// ------------------------------
document.addEventListener('DOMContentLoaded', function() {
    DatatableAdvanced.init();
});
