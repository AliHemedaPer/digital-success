export default {
    PAGINATION_ROWS: 2,
    PUSHER_CLIENT_SETTINGS: {
        broadcaster: 'pusher',
        key: '4eeeab25c3d42fb36d51',
        cluster: "mt1",
        encrypted: true,
    }
}
