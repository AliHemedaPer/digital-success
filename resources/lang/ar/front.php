<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'follow' => 'تابعنا',
    'home' => 'الرئيسية',
    'about' => 'عن النجاح الرقمي',
    'services' => 'خدماتنا',
    'products' => 'المنتجات',
    'links' => 'روابط',
    'contact' => 'اتصل بنا',
    'language' => 'Eng',
    'details' => 'التفاصيل',
    'more' => 'إقرأ المزيد',
    'services' => 'خدماتنا',
    'services_hint' => 'و ببساطة نص شكلي في صناعة الطباعة والتنضيد. لوريم إيبسوم ',
    
    'blog' => 'البلوج والمقالات',
    'address' => 'العنوان',
    'call' => 'اتصل بنا',
    'emailus' => 'راسلنا',
    'join' => 'انضم إلينا',
    'name' => 'الإسم',
    'email' => 'الإيميل',
    'subject' => 'العنوان',
    'message' => 'الموضوع',
    'submit' => 'راسلنا',
    'rights' => '2019 © جميع الحقوق محفوظه لشركة النجاح الرقمي',
    'aboutus' => 'عن النجاح الرقمي',
    'aboutus_hint' => 'قم بالتسجيل الآن باستخدام خدمة تصميمك الفوري واحفظ نفقاتك الشهرية',
    'buy' => 'اشتري الآن',
    'phone' => 'رقم الهاتف',
    'sending' => 'أرسال',
    'close' => 'غلق',
    'send' => 'أرسل',
    'view' => 'عرض',
    'joinus' => 'إنضم <span>الينا</span>',
    'joinus_hint' => 'قم بالتسجيل الآن باستخدام خدمة تصميمك الفوري واحفظ نفقاتك الشهرية',
    'job' => 'عنوان الوظيفه',
    'attach' => 'إرفاق',
    'send' => 'أرسل',
    'category' => 'القسم الرئيسي',
    'subcategory' => 'القسم الفرعي',
    'filter' => 'بحث',
    'all' => 'الكل',
    'who_we_are' => 'من نحن؟',
    'why_us' => 'لماذا النجاح الرقمي؟',
    'service_agreement' => 'إتفاقية الخدمة',
    'privacy_policy' => 'سياسة الخصوصية',
    'payments' => 'طرق الدفع',
    'message_sent' => 'تم الإرسال بنجاح!',
    'message_not_sent' => 'فشل الإرسال, برجاء المحاولة لاحقا!',
    'select_service' => 'إختر نوع الخدمة',
    'other_service' => 'خدمات أخري',
    'request' => 'طلب',
    'request_quote' => 'طلب خدمة',
    'menu' => 'القائمة',
    'guidance' => 'أفضل الإرشادات'
];
