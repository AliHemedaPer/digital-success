<?php

return [    
    'dashboard' => 'المنصة',
    'categories' => 'الأقسام',
    'services' => 'الخدمات',
    'courses' => 'الكورسات',
    'pages' => 'الصفحات',
    'blogs' => 'البلوج',
    'admins' => 'المديرين',
    'clients' => 'العملاء',
    'main' => 'الرئيسية',
    'add_new' => 'إضافة',
    'service' => 'خدمة',
    'page' => 'صفحة',
    'course' => 'كورس',
    'blog' => 'منشور',
    'about' => 'عن النجاح الرقمي',
    'ecommerce' => 'المتاجر الإليكترونية',
    'banners' => 'البنرات',
    'banner' => 'بانر',
    'counters' => 'العدادات',
    'counter' => 'عداد',
    'count' => 'العدد',

    'category' => 'قسم',
    'video' => 'رابط فيديو',
    'short_desc' => 'وصف مختصر',
    'content' => 'الوصف',
    'permissions' => 'الصلاحيات',
    'info' => 'البيانات',
    'general_stin' => 'الإعدادات العامة',

    'nextLang' => 'عربى',
    'nextLangCode' => 'ar',
    'accountSettings' => 'إعدادات الحساب',
    'changePassword' => 'تغيير الرقم السري',
    'logout' => 'تسجيل الخروج',
    
    'add' => 'إضافة',
    'read' => 'قراءة',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'admin' => 'مدير',
    'parent' => 'القسم الرئيسي',
    
    'actions' => 'الحدث',
    
    'inactive' => 'غير مفعل',
    'active' => 'مفعل',
    'description' => 'الوصف',
    'status' => 'الحالة',
    'image' => 'الصورة',
    
    'title' => 'العنوان',
    'save' => 'حفظ',
    'cancel' => 'إلغاء',
    
    'client' => 'العميل',
    
    'clientName' => 'إسم العميل',
    'clientPhone' => 'تليفون العميل',
    'clientAddress' => 'عنوان العميل',
    
    'close' => 'غلق',
    'available' => 'متاح',
    
    'confirmNewPassword' => 'تأكيد الرقم السري الجديد',
    'newPassword' => 'الرقم السري الجديد',
    'currentPassword' => 'الرقم السري الحالي',
    'password' => 'الرقم السري ',
   
    'Month' => 'شهر',
    'Year' => 'سنة',
    'Show' => 'عرض',
    
    'details' => 'التفاصيل',
    'name' => 'الإسم',
    'phone' => 'رقم الهاتف',
    'email' => 'البريد الإليكتروني',
    'logo' => 'الشعار',
    'reset_password' => 'تغيير الرقم السري',
    'your_account_email' => 'البريد الإليكتروني',
    'reset' => 'تغيير',
    'login_to_your_account' => 'الدخول إلي حسابك',
    'remember' => 'تذكرني',
    'your_credentials' => 'بيانات المرور',
    'forget_pass' => 'نسيت الرقم السري؟',
    'login' => 'دخول',

    'app_name' => 'إسم الشركة',
    'app_email' => 'إيميل التواصل',
    'app_form_email' => 'إيميل الفورم',
    'app_address' => 'عنوان الشركة',
    'fb' => 'صفحة الفيسبوك',
    'tw' => 'صفحة تويتر',
    'in' => 'صفحة لينكدإن'

];