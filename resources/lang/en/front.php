<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'follow' => 'Follow US',
    'home' => 'Home',
    'about' => 'About Us',
    'services' => 'Services',
    'products' => 'Products',
    'links' => 'Links',
    'contact' => 'Contact Us',
    'language' => 'عربي',
    'details' => 'Details',
    'more' => 'Read More',
    'services' => 'Our <span>services</span>',
    'services_hint' => 'Suspendisse sed eros mollis, tincidunt felis eget, interdum erat. <br/>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    
    'blog' => 'Blog & Articles',
    'address' => 'Address',
    'call' => 'Call Us',
    'emailus' => 'Email Us',
    'join' => 'Join us',
    'name' => 'Name',
    'email' => 'Email',
    'subject' => 'Subject',
    'message' => 'Message',
    'submit' => 'Submit',
    'rights' => '2019 © Digital Agency All Rights Reserved',
    'aboutus' => 'About <span>Us</span>',
    'aboutus_hint' => 'Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. ',
    'buy' => 'Buy Now',
    'phone' => 'Phone',
    'sending' => 'Sending',
    'close' => 'Close',
    'send' => 'Send',
    'view' => 'View',
    'joinus' => 'Join <span>us</span>',
    'joinus_hint' => 'Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.',
    'job' => 'Job Title',
    'attach' => 'Attach',
    'send' => 'Send',
    'category' => 'Category',
    'subcategory' => 'Sub Category',
    'filter' => 'Filter',
    'all' => 'All',
    'who_we_are' => 'Who We Are?',
    'why_us' => 'Why Digital Success?',
    'service_agreement' => 'Service Agreement',
    'privacy_policy' => 'Privacy Policy',
    'payments' => 'Payments',
    'message_sent' => 'Message Sent Sucessfully!',
    'message_not_sent' => 'Message Failed, Please Try Later!',
    'select_service' => 'Select Service',
    'other_service' => 'Other Service',
    'request' => 'Request',
    'request_quote' => 'Request A Quote',
    'menu' => 'Menu',
    'guidance' => 'You Always Get the Best Guidance'
];
