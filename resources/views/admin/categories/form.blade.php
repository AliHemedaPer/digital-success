@push('scripts')
     <!-- Theme JS files -->
     <script src="{{asset('public/backend/js/plugins/forms/selects/select2.min.js')}}"></script>    
     <script src="{{asset('public/backend/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>   
    <!-- /theme JS files -->
     <script type="text/javascript">    
         // Validation config
        var FormValidation = function() {

            // Summernote
            var _componentSummernote = function() {
                if (!$().summernote) {
                    console.warn('Warning - summernote.min.js is not loaded.');
                    return;
                }

                // Basic examples
                // ------------------------------

                // Default initialization
                $('.summernote').summernote({
                    height: 200
                });

            };

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }
                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };    

            // Select2 select
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                var $select = $('.form-control-select2').select2({
                    
                });

                // Trigger value change when selection is made
                $select.on('change', function() {
                    $(this).trigger('blur');
                });


                var $selectCategory = $('.form-control-select2-parent').select2({
                    
                });

                // Trigger value change when selection is made
                $selectCategory.on('change', function() {
                    $(this).trigger('blur');
                });


                $(".form-control-select2-parent").data('select2').trigger('select', {
                    data: {"id": '{{ isset($category) ? $category->parent : ''}}' }
                });
            };

            return {
                init: function() {
                    _componentSummernote();
                    _componentUniform();
                    _componentSelect2();
                    //_componentValidation();
                }
            }
        }();
        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
           
        });
    </script>
@endpush
<form method="POST" action="{{url('admin/categories'). (isset($category) ? ('/' . $category->id) : '')}}" class="form__init" enctype="multipart/form-data">
    @if(isset($category))
    {{ method_field('PATCH') }}
    @endif
    @csrf
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <div class="form-group col-md-12" >
                    <div class="form-group">
                        <label>@lang('admin.parent'):</label>
                        <select data-placeholder="Select Parent" name="parent" id="parent" class="form-control form-control-select2-parent select-search" data-fouc>
                            <option value="0">---- بدون ----</option>
                            @foreach($parents as $parent)
                                <option value="{{ $parent->id }}" {{ (isset($category) && $category->parent == $parent->id) ?'selected=""':'' }}>{{ $parent->name_ar }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <div class="form-group col-md-12" >
                    <label>@lang('admin.title') (EN):</label>
                    <input type="text" class="form-control" name="name_en"
                           value="{{isset($category) ? $category->name_en: old('name_en') }}" required>
                </div>

                <div class="form-group col-md-12">
                    <label>@lang('admin.image'):</label>
                    <input type="file" name="image" class="form-control-uniform-custom">
                </div>
                <div class="card-img-actions m-1 col-md-6">
                    @if(isset($category) && $category->image)
                    <img class="card-img img-fluid" src="{{ asset('public/images/categories/'.$category->image) }}" alt="">
                    @else
                    <img class="card-img img-fluid" src="{{ asset('public/images/no-photo.png') }}" alt="" style="max-width: 100px">
                    @endif
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <div class="form-group col-md-12" >
                    <label>@lang('admin.title') (AR):</label>
                    <input type="text" class="form-control" name="name_ar"
                           value="{{isset($category) ? $category->name_ar: old('name_ar') }}" required>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <div class="form-group col-md-12">
                    <label>ظهور في القائمة الرئيسية:</label>
                    <input type="checkbox" name="home_page" {!! (isset($category) && $category->home_page ) ? ' checked="checked"' : '' !!}>
                </div>
            </fieldset>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <div class="form-group">
                    <label>@lang('admin.content') (EN):</label>
                    <textarea rows="4" id="content_en" class="summernote" name="content_en" 
                        required>{{isset($category) ? $category->content_en : old('content_en')}}</textarea>
                </div>
                
                <div class="form-group">
                    <label>@lang('admin.content') (AR):</label>
                    <textarea rows="4" id="content_ar" class="summernote" name="content_ar" 
                        required>{{isset($category) ? $category->content_ar : old('content_ar')}}</textarea>
                </div>
            </fieldset>
        </div>
    </div>

    <div class="d-flex justify-content-end align-items-center">
        <button type="button" onclick="App.redirect('/admin/categories');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
        <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
    </div>
</form>
