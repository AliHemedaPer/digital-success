@extends('layouts.admin.master')

@section('title')
    {{ $appSettings->app_name }} - admin - Change Password
@endsection

@push('scripts')
@endpush

@section('header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i> @lang('admin.changePassword')</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- 2 columns form -->
    <div class="card">
        <!-- <div class="card-header header-elements-inline">
            &nbsp;
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div> -->

        <div class="card-body">
            <form method="POST" action="{{url('admin/chpassword')}}" class="form__init"  enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.currentPassword') *:</label>
                                <input type="password" class="form-control" name="password"
                                     required>
                            </div>

                            <div class="form-group">
                                <label>@lang('admin.newPassword') *:</label>
                                <input type="password" class="form-control" name="npassword"
                                     required>
                            </div>

                            <div class="form-group">
                                <label>@lang('admin.confirmNewPassword') *:</label>
                                <input type="password" class="form-control" name="cpassword"
                                     required>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /2 columns form -->
@endsection
