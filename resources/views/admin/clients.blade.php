@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'company name') !!} - Admin -
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/demo_pages/form_checkboxes_radios.js')}}"></script>
    
    <script src="{{asset('public/backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script src="{{asset('public/backend/js/components/datatables.js')}}?target=.datatable-main&columnsNum=5&valueNum={{$clients->count()}}"></script>
    <!-- /theme JS files -->
@endpush

@section('styles')
.uniform-choice {
    margin: auto !important;
}
@endsection

@section('content')
    <!-- Page length options -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title"><i class="icon-circle-right2 mr-2"></i> @lang('admin.clients')</h5>
        </div>

        <table class="table datatable-main">
            <thead>
                <tr>
                    <th>@lang('admin.name')</th>
                    <th>@lang('admin.email')</th>
                    <th>@lang('admin.phone')</th>
                    <!-- <th class="text-center">Active</th>
                    <th class="text-center">Inactive</th>
                    <th class="text-center">Can't Request</th> -->
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                    <tr id="row_{{ $client->id }}">
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->phone }}</td>
                        <!-- <td class="text-center">
                            <input class="status_radio form-check-input-styled" type="radio" name="client_{{ $client->id }}" data-id="{{ $client->id }}" value="1" {!! $client->status == 1 ? 'checked=""':'' !!} >
                        </td>
                        <td class="text-center">
                            <input class="status_radio form-check-input-styled" type="radio" name="client_{{ $client->id }}" data-id="{{ $client->id }}" value="0" {!! $client->status == 0 ? 'checked=""':'' !!}>
                        </td>
                        <td class="text-center">
                            <input class="status_radio form-check-input-styled" type="radio" name="client_{{ $client->id }}" data-id="{{ $client->id }}" value="-1" {!! $client->status == -1 ? 'checked=""':'' !!}>
                        </td> -->
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
     <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
    <!-- /page length options -->
@endsection

@section('jquery')
<script type="text/javascript">
    var _token = $('#_token').val();
    $(document).ready(function(){
        $('.status_radio').click(function(){
            let cl  = $(this).attr('data-id');
            let sts = $(this).val();
            $.ajax({
                url:'{{ URL::to('admin/client')}}/'+cl+'/'+sts,
                type:'POST',
                data:'_token='+_token,
                success: function(alerts){
                    //alert(alerts);
                }
            });
        });
    });
</script>
@endsection