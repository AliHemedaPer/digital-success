 @extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'company name') !!} - Admin - 
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/media/fancybox.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/ui/moment/moment.min.js')}}"></script>
    <!-- /theme JS files -->
    <script type="text/javascript">
        var Lightbox = function() {
            var _componentFancybox = function() {
                if (!$().fancybox) {
                    console.warn('Warning - fancybox.min.js is not loaded.');
                    return;
                }
                // Image lightbox
                $('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            };
            return {
                init: function() {
                    _componentFancybox();
                }
            }
        }();

         // Validation config
        var FormComponents = function() {
            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }

                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };
            
            // Daterange picker
            var _componentDaterange = function() {
                if (!$().daterangepicker) {
                    console.warn('Warning - daterangepicker.js is not loaded.');
                    return;
                }

                // Single picker
                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    minDate: '{{ date('Y/m/d') }}',
                    locale: {
                        // formatSubmit: 'Y-M-D',
                        format: 'Y/M/D',
                    }
                });
            };

            var _componentValidation = function() {
                if (!$().validate) {
                    console.warn('Warning - validate.min.js is not loaded.');
                    return;
                }
                // Initialize
                var validator = $('.form__init').validate({
                    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                    errorClass: 'validation-invalid-label',
                    successClass: 'validation-valid-label',
                    validClass: 'validation-valid-label',
                    highlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    success: function(label) {
                        label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
                    },

                    // Different components require proper error label placement
                    errorPlacement: function(error, element) {
                        // Unstyled checkboxes, radios
                        if (element.parents().hasClass('form-check')) {
                            error.appendTo( element.parents('.form-check').parent() );
                        }

                        // Input with icons and Select2
                        else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                            error.appendTo( element.parent() );
                        }

                        // Input group, styled file input
                        else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                            error.appendTo( element.parent().parent() );
                        }

                        // Other elements
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        quantity: {
                            number: true,
                            min: 1,
                        },
                        selling_price: {
                            number: true,
                        }
                    },
                    messages: {
                        quantity: {
                            min: 'Please select at least {0} quantity'
                        }
                    }
                });
            };
            return {
                init: function() {
                    _componentUniform();
                    _componentDaterange();
                    //_componentValidation();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function() {
            FormComponents.init();
            Lightbox.init();
        });
    </script>
@endpush

@section('header')

@endsection

@section('content')
<form method="POST" action="{{url('admin/counters'). (isset($counter) ? ('/' . $counter->id) : '')}}" class="form__init" enctype="multipart/form-data">
    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{ isset($counter) ? __('admin.edit') : __('admin.add') }} {{ __('admin.banner') }}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <button type="button" onclick="App.redirect('/admin/counters');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body">
                @if(isset($counter))
                    {{ method_field('PATCH') }}
                @endif
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title')  (EN):</label>
                                <input type="text" class="form-control" name="title_en"
                                    value="{{isset($counter) ? $counter->title_en: old('title_en') }}" required>
                            </div>              
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title') (AR):</label>
                                <input type="text" class="form-control" name="title_ar"
                                    value="{{isset($counter) ? $counter->title_ar: old('title_ar') }}" required>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.count') (EN):</label>
                                <input type="text" class="form-control" name="count"
                                    value="{{isset($counter) ? $counter->count: old('count') }}">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.image'):</label>
                                <input type="file" name="image" class="form-control-uniform-custom">
                            </div>
                            
                            <div class="card-img-actions m-1">
                                @if(isset($counter) && $counter->image)
                                <img class="card-img img-fluid" src="{{ asset($counter->image) }}" alt="">
                                @else
                                <img class="card-img img-fluid" src="{{ asset('public/images/no-photo.png') }}" alt="" style="max-width: 100px">
                                @endif
                            </div>
                        </fieldset>
                    </div>
                </div>

                <br/>
                <div class="d-flex justify-content-end align-items-center">
                    
                        <button type="button" onclick="App.redirect('/admin/counters');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
        </div>
    </div>
    <!-- /2 columns form -->
</form>
@endsection

@section('jquery')
<script type="text/javascript">

</script>
@endsection


