@extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'home') !!}
@endsection

@section('header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i> @lang('admin.dashboard')</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <!-- <div class="header-elements d-none">
                <div class="d-flex justify-content-center">
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                </div>
            </div> -->
        </div>
    </div>
@endsection


@section('content')
    <div class="row">
                    <div class="col-xl-12">

                        <!-- Traffic sources -->
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h6 class="card-title"></h6>
                                <div class="header-elements">
                                    <div class="form-check form-check-right form-check-switchery form-check-switchery-sm">
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="card-body py-0" style="padding-bottom: 25px !important;">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="d-flex align-items-center justify-content-center mb-2">
                                            <a href="#" class="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
                                                <i class="icon-users"></i>
                                            </a>
                                            <div>
                                                <div class="font-weight-semibold">@lang('admin.clients')</div>
                                                <span class="text-muted"><span class="badge badge-mark border-success mr-2"></span>{{ $clients }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="d-flex align-items-center justify-content-center mb-2">
                                            <a href="#" class="btn bg-transparent border-warning-400 text-warning-400 rounded-round border-2 btn-icon mr-3">
                                                <i class="icon-magic-wand2"></i>
                                            </a>
                                            <div>
                                                <div class="font-weight-semibold">@lang('admin.services')</div>
                                                <span class="text-muted"><span class="badge badge-mark border-success mr-2"></span>{{ $services }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="d-flex align-items-center justify-content-center mb-2">
                                            <a href="#" class="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon mr-3">
                                                <i class="icon-stack-text"></i>
                                            </a>
                                            <div>
                                                <div class="font-weight-semibold">@lang('admin.blogs')</div>
                                                <span class="text-muted"><span class="badge badge-mark border-success mr-2"></span>{{ $blog }} </span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /traffic sources -->

                    </div>
                </div>
@endsection
