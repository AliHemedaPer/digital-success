<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{!! env('COMPANY_NAME', 'company name') !!} - Login -</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/components.min.css?i=1')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('public/backend/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('public/backend/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('public/backend/js/app.js')}}"></script>
    <script src="{{asset('public/backend/js/demo_pages/login.js')}}"></script>

</head>

<body class="bg-slate-800">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login card -->
                <form class="login-form" method="POST" action="{{ route('admin.login.submit') }}">
                    {{ csrf_field() }}
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">@lang('admin.login_to_your_account')</h5>
                                <span class="d-block text-muted">@lang('admin.your_credentials')</span>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} form-group-feedback form-group-feedback-left">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus autocomplete="off" placeholder="@lang('admin.email')">
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="validation-invalid-label">
                                        <small>{{ $errors->first('email') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} form-group-feedback form-group-feedback-left">
                                <input id="password" type="password" placeholder="@lang('admin.password')" class="form-control" name="password" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="validation-invalid-label">
                                        <small>{{ $errors->first('password') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <!-- <div class="form-check mb-0">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="remember" class="form-input-styled" checked data-fouc>
                                        Remember
                                    </label>
                                </div> -->

                                <a href="{{ url('admin/reset') }}" class="ml-auto">@lang('admin.forget_pass')</a>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">@lang('admin.login') <i class="icon-circle-left2 ml-2"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /login card -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</body>
</html>
