 @extends('layouts.admin.master')

@section('title')
    {!! env('COMPANY_NAME', 'company name') !!} - Admin - 
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/media/fancybox.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script src="{{asset('public/backend/js/plugins/editors/summernote/summernote.min.js')}}"></script>

    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/ui/moment/moment.min.js')}}"></script>
    <!-- /theme JS files -->
    <script type="text/javascript">
        var Lightbox = function() {
            var _componentFancybox = function() {
                if (!$().fancybox) {
                    console.warn('Warning - fancybox.min.js is not loaded.');
                    return;
                }
                // Image lightbox
                $('[data-popup="lightbox"]').fancybox({
                    padding: 3
                });
            };
            return {
                init: function() {
                    _componentFancybox();
                }
            }
        }();

         // Validation config
        var FormComponents = function() {

            // Summernote
            var _componentSummernote = function() {
                if (!$().summernote) {
                    console.warn('Warning - summernote.min.js is not loaded.');
                    return;
                }

                // Basic examples
                // ------------------------------

                // Default initialization
                $('.summernote').summernote({
                    height: 300
                });

            };

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }

                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };
            
            // Daterange picker
            var _componentDaterange = function() {
                if (!$().daterangepicker) {
                    console.warn('Warning - daterangepicker.js is not loaded.');
                    return;
                }

                // Single picker
                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    minDate: '{{ date('Y/m/d') }}',
                    locale: {
                        // formatSubmit: 'Y-M-D',
                        format: 'Y/M/D',
                    }
                });
            };
            // Select2 select
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                var $select = $('.form-control-select2').select2({
                    
                });

                // Trigger value change when selection is made
                $select.on('change', function() {
                    $(this).trigger('blur');
                });


                var $selectCategory = $('.form-control-select2-category').select2({
                    
                });

                /*var $selectCategory = $('.form-control-select2-post').select2({
                    
                });*/

                // Trigger value change when selection is made
            
                $selectCategory.on('change', function() {
                    $(this).trigger('blur');
                });


                $(".form-control-select2-category").data('select2').trigger('select', {
                    data: {"id": '{{ isset($post) ? $post->category : ''}}' }
                });

                /*$(".form-control-select2-post").data('select2').trigger('select', {
                    data: {"id": '{{ (isset($post) ? $post->post_type : (isset($post_type) ? $post_type : '')) }}' }
                });*/

            };
            var _componentValidation = function() {
                if (!$().validate) {
                    console.warn('Warning - validate.min.js is not loaded.');
                    return;
                }
                // Initialize
                var validator = $('.form__init').validate({
                    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                    errorClass: 'validation-invalid-label',
                    successClass: 'validation-valid-label',
                    validClass: 'validation-valid-label',
                    highlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    success: function(label) {
                        label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
                    },

                    // Different components require proper error label placement
                    errorPlacement: function(error, element) {
                        // Unstyled checkboxes, radios
                        if (element.parents().hasClass('form-check')) {
                            error.appendTo( element.parents('.form-check').parent() );
                        }

                        // Input with icons and Select2
                        else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                            error.appendTo( element.parent() );
                        }

                        // Input group, styled file input
                        else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                            error.appendTo( element.parent().parent() );
                        }

                        // Other elements
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        quantity: {
                            number: true,
                            min: 1,
                        },
                        selling_price: {
                            number: true,
                        }
                    },
                    messages: {
                        quantity: {
                            min: 'Please select at least {0} quantity'
                        }
                    }
                });
            };
            return {
                init: function() {
                    _componentUniform();
                    _componentDaterange();
                    _componentSelect2();
                    _componentSummernote();
                    //_componentValidation();
                }
            }
        }();

        var FormComponents2 = function() {
            // Select2 select
            var _componentSelect2 = function() {
                if (!$().select2) {
                    console.warn('Warning - select2.min.js is not loaded.');
                    return;
                }

                // Initialize
                var $select = $('.form-control-select2').select2({
                    
                });

                // Trigger value change when selection is made
                $select.on('change', function() {
                    $(this).trigger('blur');
                });

                // Trigger value change when selection is made
                $selectMaterial.on('change', function() {
                    $(this).trigger('blur');
                });

                $selectUnit.on('change', function() {
                    $(this).trigger('blur');
                });
            };
            return {
                init: function() {
                    _componentSelect2();
                }
            }
        }();

        document.addEventListener('DOMContentLoaded', function() {
            FormComponents.init();
            Lightbox.init();
        });

    function removeTr(id, typ){
        $('#'+typ+'_'+id).remove();
    }
    </script>
@endpush

@section('header')

@endsection

@section('content')
<form method="POST" action="{{url('admin/posts'). (isset($post) ? ('/' . $post->id) : '')}}" class="form__init" enctype="multipart/form-data">
    <!-- 2 columns form -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{ isset($post) ? __('admin.edit') : __('admin.add') }} {{ __('admin.'.$post_type) }}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    @if($post_type == 'page')
                        <button type="button" onclick="App.redirect('/admin/home');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    @else
                        <button type="button" onclick="App.redirect('/admin/posts?pst={{ $post_type }}');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    @endif
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
            </div>
        </div>

        <div class="card-body">
                @if(isset($post))
                    {{ method_field('PATCH') }}
                @endif
                @csrf
                <input type="hidden" name="post_type" value="{{ (isset($post) ? $post->post_type : (isset($post_type) ? $post_type : '')) }}">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title')  (EN):</label>
                                <input type="text" class="form-control" name="title_en"
                                    value="{{isset($post) ? $post->title_en: old('title_en') }}" required>
                            </div>
                            
                            <div class="form-group">
                                <label>@lang('admin.category'):</label>
                                <select data-placeholder="Select Category" name="category" id="category" class="form-control form-control-select2-category select-search" data-fouc>
                                    <option></option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ (isset($post) && $post->category == $category->id) ?'selected=""':'' }}>{{ $category->name_ar }} </option>
                                    @endforeach
                                </select>
                            </div>                          
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.title') (AR):</label>
                                <input type="text" class="form-control" name="title_ar"
                                    value="{{isset($post) ? $post->title_ar: old('title_ar') }}" required>
                            </div>


                            <div class="form-group">
                                <label>@lang('admin.video'):</label>
                                <input type="text" class="form-control" name="video"
                                    value="{{isset($post) ? $post->video: old('video') }}">
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.short_desc') (EN):</label>
                                <textarea rows="4" id="short_desc" class="form-control" name="short_desc_en" 
                                    >{{isset($post) ? $post->short_desc_en : old('short_desc_en')}}</textarea>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            
                            <div class="form-group">
                                <label>@lang('admin.short_desc') (AR):</label>
                                <textarea rows="4" id="composition" class="form-control" name="short_desc_ar" 
                                    >{{isset($post) ? $post->short_desc_ar : old('short_desc_ar')}}</textarea>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.content') (EN):</label>
                                <textarea rows="4" id="content_en" class="summernote" name="content_en" 
                                    required>{{isset($post) ? $post->content_en : old('content_en')}}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>@lang('admin.content') (AR):</label>
                                <textarea rows="4" id="content_ar" class="summernote" name="content_ar" 
                                    required>{{isset($post) ? $post->content_ar : old('content_ar')}}</textarea>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.image'):</label>
                                <input type="file" name="image" class="form-control-uniform-custom">
                            </div>
                            
                            <div class="card-img-actions m-1">
                                @if(isset($post) && $post->image)
                                <img class="card-img img-fluid" src="{{ asset('public/images/posts/'.$post->image) }}" alt="">
                                <div class="card-img-actions-overlay card-img">
                                    <a href="{{ asset('public/images/posts/'.$post->image) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                        <i class="icon-plus3"></i>
                                    </a>
                                </div>
                                @else
                                <img class="card-img img-fluid" src="{{ asset('public/images/no-photo.png') }}" alt="" style="max-width: 100px">
                                @endif
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label>@lang('admin.status'):</label>
                                <div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            @if(isset($post) && !$post->status)
                                                <input type="radio" class="form-check-input" value="1" name="status" required="">
                                            @else
                                                <input type="radio" class="form-check-input" value="1" checked="" name="status" required="">
                                            @endif
                                            Active
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            @if(isset($post) && !$post->status)
                                                <input type="radio" class="form-check-input" value="0" checked="" name="status" required="">
                                            @else
                                                <input type="radio" class="form-check-input" value="0" name="status" required="">
                                            @endif
                                            Inactive
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <br/>
                <div class="d-flex justify-content-end align-items-center">
                    
                    @if($post_type == 'page')
                        <button type="button" onclick="App.redirect('/admin/home');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    @else
                        <button type="button" onclick="App.redirect('/admin/posts?pst={{ $post_type }}');" class="btn btn-default mr-2">@lang('admin.cancel') <i class="fa fa-undo ml-2"></i></button>
                    @endif
                    
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
        </div>
    </div>
    <!-- /2 columns form -->
</form>
@endsection

@section('jquery')
<script type="text/javascript">

</script>
@endsection


