<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{!! env('COMPANY_NAME', 'company name') !!} - Reset Password -</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/components.min.css?i=1')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('public/backend/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('public/backend/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('public/backend/js/app.js')}}"></script>
    <script src="{{asset('public/backend/js/demo_pages/login.js')}}"></script>

</head>

<body class="bg-slate-800">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login card -->
                <form class="login-form" method="POST" action="{{ route('admin.reset.submit') }}">
                    {{ csrf_field() }}
                    <div class="card mb-0">
                        <div class="card-body">
                            @if(count($result))
                            <div class="alert alert-{{ $result['alert'] }}">
                                {{ $result['msg'] }}
                            </div>
                            @endif
                            <div class="text-center mb-3">
                                <i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">@lang('admin.reset_password')</h5>
                            </div>

                            @lang('admin.email'):
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} form-group-feedback form-group-feedback-left">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus autocomplete="off" placeholder="@lang('admin.email')">
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="validation-invalid-label">
                                        <small>{{ $errors->first('email') }}</small>
                                    </span>
                                @endif
                            </div>
                            @if(isset($_GET['reset_code']))
                            <input class="form-control" type="hidden" name="security_code" id="security_code" value="<?php echo $_GET['reset_code']; ?>" />

                            New Password:
                            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }} form-group-feedback form-group-feedback-left">
                                <input id="new_password" type="password" placeholder="New Password" class="form-control" name="new_password" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                                @if ($errors->has('new_password'))
                                    <span class="validation-invalid-label">
                                        <small>{{ $errors->first('new_password') }}</small>
                                    </span>
                                @endif
                            </div>
                            @endif

                            <div class="form-group d-flex align-items-center">
                                <a href="{{ url('admin/login') }}" class="ml-auto">@lang('admin.login')</a>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">@lang('admin.reset') <i class="icon-circle-left2 ml-2"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /login card -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</body>
</html>
