@extends('layouts.admin.master')

@section('title')
    {{ $appSettings->app_name }} - admin - Account Settings
@endsection

@push('scripts')
    <!-- Theme JS files -->
    <script src="{{asset('public/backend/js/plugins/media/fancybox.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/ui/moment/moment.min.js')}}"></script>
    <!-- /theme JS files -->

    <script type="text/javascript">

         // Validation config
        var FormValidation = function() {

            var _componentUniform = function() {
                if (!$().uniform) {
                    console.warn('Warning - uniform.min.js is not loaded.');
                    return;
                }

                // File input
                $('.form-control-uniform').uniform();

                // Custom select
                $('.form-control-uniform-custom').uniform({
                    fileButtonClass: 'action btn bg-blue',
                    selectClass: 'uniform-select bg-pink-400 border-pink-400'
                });
            };

            var _componentValidation = function() {
                if (!$().validate) {
                    console.warn('Warning - validate.min.js is not loaded.');
                    return;
                }
                // Initialize
                var validator = $('.form__init').validate({
                    ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
                    errorClass: 'validation-invalid-label',
                    successClass: 'validation-valid-label',
                    validClass: 'validation-valid-label',
                    highlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function(element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    success: function(label) {
                        label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
                    },

                    // Different components require proper error label placement
                    errorPlacement: function(error, element) {
                        // Unstyled checkboxes, radios
                        if (element.parents().hasClass('form-check')) {
                            error.appendTo( element.parents('.form-check').parent() );
                        }

                        // Input with icons and Select2
                        else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                            error.appendTo( element.parent() );
                        }

                        // Input group, styled file input
                        else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                            error.appendTo( element.parent().parent() );
                        }

                        // Other elements
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        quantity: {
                            number: true,
                            min: 1,
                        },
                        selling_price: {
                            number: true,
                        }
                    },
                    messages: {
                        quantity: {
                            min: 'Please select at least {0} quantity'
                        }
                    }
                });
            };
            return {
                init: function() {
                    _componentUniform();
                    //_componentValidation();
                }
            }
        }();
        document.addEventListener('DOMContentLoaded', function() {
            FormValidation.init();
        });
    </script>
@endpush

@section('header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i> @lang('admin.accountSettings')</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- 2 columns form -->
    <div class="card">
        <!-- <div class="card-header header-elements-inline">
            &nbsp;
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div> -->

        <div class="card-body">
            <form method="POST" action="{{url('admin/settings')}}" class="form__init"  enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6" style="float: right;">
                            <fieldset>
                                <div class="form-group">
                                    <label>@lang('admin.app_name'):</label>
                                    <input type="text" class="form-control" name="app_name"
                                        value="{{isset($settings) ? $settings->app_name: old('app_name') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.phone'):</label>
                                    <input id="description" class="form-control description" name="app_phone" value="{{isset($settings) ? $settings->app_phone : old('app_phone')}}">
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.fb'):</label>
                                    <input type="text" class="form-control" name="fb"
                                        value="{{isset($settings) ? $settings->fb: old('fb') }}">
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.tw'):</label>
                                    <input type="text" class="form-control" name="tw"
                                        value="{{isset($settings) ? $settings->tw: old('tw') }}">
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.in'):</label>
                                    <input type="text" class="form-control" name="in"
                                        value="{{isset($settings) ? $settings->in: old('in') }}">
                                </div>
                                
                        </fieldset>
                    </div>
                    <div class="col-md-6" style="float: left;">
                        <fieldset>
                                <div class="form-group">
                                    <label>@lang('admin.app_email'):</label>
                                    <input type="text" class="form-control" name="app_email"
                                        value="{{isset($settings) ? $settings->app_email: old('app_email') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.app_form_email'):</label>
                                    <input type="text" class="form-control" name="app_form_email"
                                        value="{{isset($settings) ? $settings->app_form_email: old('app_form_email') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.app_address') (EN):</label>
                                    <input type="text" class="form-control" name="app_address_en"
                                        value="{{isset($settings) ? $settings->app_address_en: old('app_address_en') }}">
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.app_address') (AR):</label>
                                    <input type="text" class="form-control" name="app_address_ar"
                                        value="{{isset($settings) ? $settings->app_address_ar: old('app_address_ar') }}">
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.logo'):</label>
                                    <input type="file" name="image" class="form-control-uniform-custom">
                                </div>
                                
                                <div class="card-img-actions m-1">
                                    @if(isset($settings) && $settings->image)
                                    <img class="card-img img-fluid" src="{{ asset($settings->image) }}" alt="" style="max-width: 200px">
                                    @else
                                    <img class="card-img img-fluid" src="{{ asset('public/images/no-photo.png') }}" alt="" style="max-width: 100px">
                                    @endif
                                </div>
                                
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-end align-items-center">
                    <button type="submit" class="btn btn-primary">@lang('admin.save') <i class="icon-floppy-disk"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /2 columns form -->
@endsection
