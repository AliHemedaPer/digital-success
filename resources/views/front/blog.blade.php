@extends('front.front')

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url({{ asset('public/images/bg_'.$bkg.'.jpg') }});">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">@lang('front.blog')</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('/') }}">@lang('front.home') <i class="ion-ios-arrow-forward"></i></a></span> <span>@lang('front.blog') </span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row">
          @foreach($posts as $post)
          <div class="col-md-6 col-lg-4 ftco-animate">
            <div class="blog-entry">
              <a href="{{ url('/post/'.$post->id) }}" class="block-20 d-flex align-items-end" style="background-image: url({{ $post->image ? asset($post->image) : asset('public/images/no-photo.png') }});">
                <div class="meta-date text-center p-2">
                  <span class="day">{{ Carbon::parse($post->updated_at)->format('d') }}</span>
                  <span class="mos">{{ Carbon::parse($post->updated_at)->format('M') }}</span>
                  <span class="yr">{{ Carbon::parse($post->updated_at)->format('Y') }}</span>
                </div>
              </a>
              <div class="text bg-white p-4">
                <h3 class="heading"><a href="{{ url('/post/'.$post->id) }}">{{ $post->title }}</a></h3>
                <p>{{ $post->short_desc ?? '.....' }}</p>
                <div class="d-flex align-items-center mt-4">
                  <p class="mb-0"><a href="{{ url('/post/'.$post->id) }}" class="btn btn-primary">@lang('front.more')</a></p>
                  
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <!-- <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
              </ul>
            </div>
          </div>
        </div> -->
      </div>
    </section>
@stop