@extends('front.front')

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url({{ asset('public/images/bg_'.$bkg.'.jpg') }});">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">{{ $post->title }}</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('/') }}">@lang('front.home') <i class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a href="{{ url('/blog') }}">@lang('front.blog') <i class="ion-ios-arrow-forward"></i></a></span> <span>{{ $post->title }} </span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
            <h2 class="mb-3">{{ $post->title }}</h2>
            @if($post->image)
            <p>
              <img src="{{ asset($post->image) }}" alt="" class="img-fluid">
            </p>
            @endif
            {!! $post->content !!}
          </div> <!-- .col-md-8 -->

          <div class="col-lg-4 sidebar ftco-animate">
            <div class="sidebar-box ftco-animate">
              <h3>@lang('front.recent')</h3>
              @foreach($posts as $post)
              <div class="block-21 mb-4 d-flex">
                <a href="{{ url('/post/'.$post->id) }}" class="blog-img mr-4" style="background-image: url({{ $post->image ? asset($post->image) : asset('public/images/no-photo.png') }});"></a>
                <div class="text">
                  <h3 class="heading"><a href="{{ url('/post/'.$post->id) }}">{{ $post->title }}</a></h3>
                  <div class="meta">
                    <div><a href="{{ url('/post/'.$post->id) }}"><span class="icon-calendar"></span> {{ Carbon::parse($post->updated_at)->format('d m Y') }}</a></div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div><!-- END COL -->
        </div>
      </div>
    </section>
@stop