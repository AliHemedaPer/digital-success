@extends("front.front")
@section("content")
<section class="hero-wrap hero-wrap-2" style="background-image: url({{ asset('public/images/bg_'.$bkg.'.jpg') }});">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">@lang('front.contact')</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('/') }}">@lang('front.home') <i class="ion-ios-arrow-forward"></i></a></span> <span>@lang('front.contact') </span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section contact-section">
      <div class="container">
        <div class="row d-flex contact-info justify-content-center">
            <div class="col-md-8">
                <div class="row ">
                  <div class="col-md-4 text-center py-4">
                    <div class="icon">
                        <span class="icon-map-o"></span>
                    </div>
                    <p><span>@lang('front.address'):</span> 
                      @if($share_locale == 'ar')
                        {{ $appSettings->app_address_ar }}
                      @else
                        {{ $appSettings->app_address_en }}
                      @endif
                      
                    </p>
                  </div>
                  <div class="col-md-4 text-center border-height py-4">
                    <div class="icon">
                        <span class="icon-mobile-phone"></span>
                    </div>
                    <p><span>@lang('front.phone'):</span> <a href="tel://{{ $appSettings->app_phone }}">
                      {{ $appSettings->app_phone }}
                    </a></p>
                  </div>
                  <div class="col-md-4 text-center py-4">
                    <div class="icon">
                        <span class="icon-envelope-o"></span>
                    </div>
                    <p><span>@lang('front.email'):</span> <a href="mailto:{{ $appSettings->app_email }}">{{ $appSettings->app_email }}</a></p>
                  </div>
                </div>
          </div>
        </div>
        <div class="row block-9 justify-content-center ">
          <div class="col-md-8 ">
           
            <form action="#" class="bg-light p-5 contact-form">
              <div class="form-group">
                <input type="text" id="quote_name" class="form-control" placeholder="{{ __('front.name') }}">
              </div>
              <div class="form-group">
                <input type="text" id="quote_email" class="form-control" placeholder="{{ __('front.email') }}">
              </div>
              <div class="form-group">
                <input type="text" id="quote_phone"  class="form-control" placeholder="{{ __('front.phone') }}">
              </div>
              <div class="form-group">
                <input type="text" id="quote_service" class="form-control" placeholder="{{ __('front.subject') }}">
              </div>
              <div class="form-group">
                <textarea name="" id="quote_message" cols="30" rows="3" class="form-control" placeholder="{{ __('front.message') }}"></textarea>
              </div>
              <div class="form-group">
                <input type="button" onclick="servicemail('contact')" value="{{ __('front.send') }}" class="btn btn-primary py-3 px-5">
              </div>
            </form>
          
          </div>
        </div>
      </div>
    </section>
@endsection
