<!doctype html>
<html dir="{{ $share_locale == 'en' ? 'ltr' : 'rtl'  }}" lang="{{ $share_locale == 'en' ? 'en' : 'ar'  }}" style="direction: {{ $share_locale == 'en' ? 'ltr' : 'rtl'  }}">
  <head>
      <title>Digital Success</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo:400,600,700">

      <link rel="stylesheet" href="{{ asset('public/css/open-iconic-bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/animate.css') }}">
      
      <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/owl.theme.default.min.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/magnific-popup.css') }}">

      <link rel="stylesheet" href="{{ asset('public/css/aos.css') }}">

      <link rel="stylesheet" href="{{ asset('public/css/ionicons.min.css') }}">
      
      <link rel="stylesheet" href="{{ asset('public/css/flaticon.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/icomoon.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
      @if($share_locale == 'ar')
      <link rel="stylesheet" href="{{ asset('public/css/style-rtl.css') }}">
      @endif

      <link rel="stylesheet" type="text/css" media="all" href="{{ asset('public/css/stellarnav.css') }}">
    </head>

    
    @if($share_locale == 'ar')
      <body style="text-align:right; font-family:'moon','Cairo', sans-serif;font-size:16px;">
    @else
      <body style="text-align: left;">
    @endif
    
        <div class="bg-top navbar-light">
          <div class="container">
            <div class="row no-gutters d-flex align-items-center align-items-stretch">
              <div class="col-md-4 d-flex align-items-center py-4">
                <a class="navbar-brand" href="{{ url('/') }}">
                  @if($appSettings->image)
                  <img src="{{ asset($appSettings->image) }}" alt="" style="max-width: 150px">
                  @else
                  {{ $appSettings->app_name }}
                  @endif
                  
                </a>
              </div>
              <div class="col-lg-8 d-block">
                <div class="row d-flex">
                  <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
                    <div class="icon d-flex justify-content-center align-items-center"><!--<span class="icon-paper-plane"></span></div>
                     <div class="text">
                      <span>Email</span>
                      <span>youremail@email.com</span>
                    </div> -->
                  </div>
                  <div class="col-md topper d-flex align-items-center justify-content-end">
                    <div class="icon d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
                    <div class="text">
                      <span>Call</span>
                      <span style="direction: ltr;">{{ $appSettings->app_phone }}</span>
                    </div>
                  </div>
                  <!-- <div class="col-md topper d-flex align-items-center justify-content-end">
                    <p class="mb-0 d-block">
                      <a href="#" class="btn py-2 px-3 btn-primary">
                        <span>Free Consulting</span>
                      </a>
                    </p>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>



        

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
          <div class="container d-flex align-items-center">
            <form action="#" class="searchform order-lg-last">
              <div class="">
                <div class="btn-group" role="group">
                  <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if($share_locale == 'ar')
                      AR
                    @else
                      EN
                    @endif
                  </button>
                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="width: 62px;min-width: 62px;z-index: 9999;direction: rtl;">
                    
                    @if($share_locale == 'ar')
                      <a class="dropdown-item" href="javascript://" onclick="changeLang('en')">EN</a>
                    @else
                      <a class="dropdown-item" href="javascript://" onclick="changeLang('ar')">AR</a>
                    @endif
                    
                  </div>
                </div>
              </div>
            </form>

            <div class="stellarnav">
              <ul>
                  <li><a href="{{ url('/') }}" class="{{ $share_locale == 'en' ? 'pl-0' : 'pr-0' }}">@lang('front.home')</a></li>
                  <li class="{{ $share_locale == 'en' ? '' : 'drop-left' }}">
                    <a href="#">@lang('front.about')</a>
                    <ul>
                      <li><a href="{{ url('/pg/who_we_are') }}">@lang('front.who_we_are')</a></li>
                      <li><a href="{{ url('/pg/why_us') }}">@lang('front.why_us')</a></li>
                      <li><a href="{{ url('/blog') }}">@lang('front.blog')</a></li>
                      
                    </ul>
                  </li>

                  <li class="{{ $share_locale == 'en' ? '' : 'drop-left' }}"><a href="#">@lang('front.services')</a>
                    <ul>
                      @foreach($appCategories as $category)
                        @if($category->home_page == 0)
                        <li  class="{{ $share_locale == 'en' ? '' : 'drop-left'  }}">
                          <a href="{{ url('/services/'.$category->id) }}">
                            @if($share_locale == 'ar')
                              {{ $category->name_ar }}
                            @else
                              {{ $category->name_en }}
                            @endif
                          </a>
                            @php ($i = 0)
                            @foreach($appServices as $service)
                                  @if($service->category == $category->id)
                                    
                                    @if($i == 0)
                                       <ul>
                                    @endif
                                    @php ($i = 1)
                                    <li>
                                      <a href="{{ url('/service/'.$service->id) }}">
                                        @if($share_locale == 'ar')
                                          {{ $service->title_ar }}
                                        @else
                                          {{ $service->title_en }}
                                        @endif
                                      </a>
                                    </li>

                                  @endif
                              
                            @endforeach
                            @if($i)
                               </ul>
                            @endif
                        </li>
                        @endif
                      @endforeach
                      
                    </ul>
                  </li>
                  @foreach($appCategories as $category)
                      @if($category->home_page == 1)
                      <li  class="{{ $share_locale == 'en' ? '' : 'drop-left' }}">
                        <a href="{{ url('/services/'.$category->id) }}">
                          @if($share_locale == 'ar')
                            {{ $category->name_ar }}
                          @else
                            {{ $category->name_en }}
                          @endif
                        </a>
                        @php ($j = 0)
                        @foreach($appServices as $service)
                           
                          @if($service->category == $category->id)
                            
                            @if($j == 0)
                               <ul>
                            @endif
                            @php ($j = 1)
                            <li>
                              <a href="{{ url('/service/'.$service->id) }}">
                                @if($share_locale == 'ar')
                                  {{ $service->title_ar }}
                                @else
                                  {{ $service->title_en }}
                                @endif
                              </a>
                            </li>
                          @endif
                           
                        @endforeach
                        @if($j)
                           </ul>
                        @endif
                      </li>
                     @endif
                  @endforeach   
                  
                  <li><a href="{{ url('/contact') }}">@lang('front.contact')</a></li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- END nav -->

        <!-- end header -->
        @yield("content")

    <!-- footer -->
    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-3">
            <div class="ftco-footer-widget mb-5">
              <h2 class="ftco-heading-2">@lang('front.contact')</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">
                    @if($share_locale == 'ar')
                        {{ $appSettings->app_address_ar }}
                      @else
                        {{ $appSettings->app_address_en }}
                      @endif
                  </span></li>
                  <li><a href="tel://{{ $appSettings->app_phone }}"><span class="icon icon-phone"></span><span class="text">{{ $appSettings->app_phone }}</span></a></li>
                  <li><a href="mailto:{{ $appSettings->app_email }}"><span class="icon icon-envelope"></span><span class="text">{{ $appSettings->app_email }}</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="ftco-footer-widget mb-5 ml-md-4">
              <h2 class="ftco-heading-2">@lang('front.links')</h2>
              <ul class="list-unstyled">
                <li><a href="{{ url('/') }}">@lang('front.home')</a></li>
                <li><a href="{{ url('/pg/who_we_are') }}">@lang('front.who_we_are')</a></li>
                <li><a href="{{ url('/contact') }}">@lang('front.contact')</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="ftco-footer-widget mb-5 ml-md-4">
              <h2 class="ftco-heading-2">&nbsp;</h2>
              <ul class="list-unstyled">
                <li><a href="{{ url('/pg/payments') }}">@lang('front.payments')</a></li>
               <li><a href="{{ url('/pg/privacy_policy') }}">@lang('front.privacy_policy')</a></li>
               <li><a href="{{ url('/pg/service_agreement') }}">@lang('front.service_agreement')</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="ftco-footer-widget mb-5">
              <h2 class="ftco-heading-2 mb-0">@lang('front.follow')</h2>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                @if($appSettings->tw)
                <li class="ftco-animate"><a href="{{$appSettings->tw}}" target="_blank"><span class="icon-twitter"></span></a></li>
                @endif

                @if($appSettings->fb)
                <li class="ftco-animate"><a href="{{$appSettings->fb}}" target="_blank"><span class="icon-facebook"></span></a></li>
                @endif

                @if($appSettings->in)
                <li class="ftco-animate"><a href="{{$appSettings->in}}" target="_blank"><span class="icon-linkedin"></span></a></li>
                @endif
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <p>@lang('front.rights')</p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <!-- <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div> -->

  <div id="ftco-loader" class="show" style="display: none;"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <div id="ftco-msgsent" class="ftco-custom show" style="display: none;">
    @lang('front.message_sent')
  </div>

  <div id="ftco-msgnotsent" class="ftco-custom show" style="display: none;">
    @lang('front.message_not_sent')
  </div>

  <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
  <script type="text/javascript" src="{{ asset('public/js/stellarnav.min.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      jQuery('.stellarnav').stellarNav({
        theme: 'dark',
        breakpoint: 960,
        menuLabel: "{{ __('front.menu') }}",
        openingSpeed: 50, // how fast the dropdown should open in milliseconds
        closingDelay: 10,

        position: 'right',
        phoneBtn: '{{ $appSettings->app_phone }}',
        phoneLabel: 'Call Us',
        locationBtn: ''
      });
    });
  </script>
  <!-- required -->

  <!-- <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script> -->

  <!-- <script src="{{ asset('public/js/jquery.min.js') }}"></script> -->
  <script src="{{ asset('public/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('public/js/popper.min.js') }}"></script>
  <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('public/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('public/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('public/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('public/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('public/js/aos.js') }}"></script>
  <script src="{{ asset('public/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('public/js/scrollax.min.js') }}"></script>
  <script src="{{ asset('public/js/main.js') }}"></script>
    
  </body>

<script type="text/javascript">

  function changeLang(lang){
    $.ajax({
      url: "{{url('/')}}/locale/"+lang,
      type: 'GET',
      success: function (data) {
         location.reload();
        }
      });
  }

function servicemail(typ){
  let title   = $('#quote_service').val();
  let name   = $('#quote_name').val();
  let email   = $('#quote_email').val();
  let phone   = $('#quote_phone').val();
  let message = $('#quote_message').val();

  if(title == '0' || title == '' || name == '' || email == '' || phone == '' || message == ''){
    alert('All fileds are required!');
    return;
  }
  $('#ftco-loader').fadeIn(100);

  $.ajax({
    url: "{{url('/')}}/servicemail",
    type: 'POST',
    data:{title:title, name:name, email:email, phone:phone, message:message, typ:typ },
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (data) {
       $('#ftco-loader').fadeOut(100);

       $('#ftco-msgsent').fadeIn(100);
       setTimeout(function(){ 
        $('#ftco-msgsent').fadeOut(100);
       }, 3000);

      },
      error: function (jqXHR, exception) {
        $('#ftco-msgnotsent').fadeIn(100);
       setTimeout(function(){ 
        $('#ftco-msgnotsent').fadeOut(100);
       }, 3000);
      }
  });
}
</script>

    <!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+200000000000", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "left", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>

</html>
