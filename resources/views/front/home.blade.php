@extends("front.front")
@section("content")
	<section class="home-slider owl-carousel" style="direction: ltr;">
      @foreach($banners as $banner)
      <div class="slider-item" style="background-image:url({{ url($banner->image) }});">
        <div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-7 ftco-animate">
            @if($banner->short_desc)
            <span class="subheading">{{ $banner->short_desc }}</span>
            @endif

            @if($banner->title)
            <h1 class="mb-4">{{ $banner->title }}</h1>
            @endif
            <!-- <p><a href="#" class="btn btn-primary px-4 py-3 mt-3">Our Services</a></p> -->
          </div>
        </div>
        </div>
      </div>
      @endforeach
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-5 order-md-last wrap-about align-items-stretch">
            <div class="wrap-about-border ftco-animate">
              <div class="img" style="background-image: url(images/about.jpg); border"></div>
              <div class="text">
                <h3>Read Our Success Story for Inspiration</h3>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word.</p>
                <p><a href="#" class="btn btn-primary py-3 px-4">Contact us</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-7 wrap-about pr-md-4 ftco-animate">
            <h2 class="mb-4">Our Main Features</h2>
            <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word.</p>
            <div class="row mt-5">
              <div class="col-lg-6">
                <div class="services active text-center">
                  <div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-collaboration"></span></div>
                  <div class="text media-body">
                    <h3>Organization</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                  </div>
                </div>
                <div class="services text-center">
                  <div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-analysis"></span></div>
                  <div class="text media-body">
                    <h3>Risk Analysis</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="services text-center">
                  <div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-search-engine"></span></div>
                  <div class="text media-body">
                    <h3>Marketing Strategy</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                  </div>
                </div>
                <div class="services text-center">
                  <div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-handshake"></span></div>
                  <div class="text media-body">
                    <h3>Capital Market</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="ftco-intro ftco-no-pb img" style="background-image: url({{ asset('public/images/bg_6.jpg') }}">
        <div class="container">
          <div class="row justify-content-center mb-5">
            <div class="col-md-10 text-center heading-section heading-section-white ftco-animate">
              <h2 class="mb-0">@lang('front.guidance')</h2>
            </div>
          </div>  
        </div>
      </section>

    <section class="ftco-counter" id="section-counter">
        <div class="container">
          <div class="row d-md-flex align-items-center justify-content-center">
            <div class="wrapper">
              <div class="row d-md-flex align-items-center">
                @foreach($counters as $counter)
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                  <div class="block-18">
                    <div class="icon"><span class="flaticon-doctor"></span></div>
                    <div class="text">
                      <strong class="number" data-number="{{ $counter->count }}">0</strong>
                      <span>{{ $counter->title }}</span>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">@lang('front.services')</h2>
            
          </div>
        </div>
        <div class="row no-gutters">

          @foreach($appCategories as $category)
            <div class="col-lg-4 d-flex">
              <a href="{{ url('/services/'.$category->id) }}">
                <div class="services-2 {{ ($loop->index == 0 ||  $loop->index % 3 == 0) ? 'noborder-left' : '' }}  text-center ftco-animate">
                  <div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-analysis"></span></div>
                  <div class="text media-body">
                    <h3>
                      @if($share_locale == 'ar')
                        {{ $category->name_ar }}
                      @else
                        {{ $category->name_en }}
                      @endif
                    </h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                  </div>
                </div>
              </a>
            </div>
          @endforeach
        </div>
      </div>
  </section>
    


    <section class="ftco-section ftco-consult ftco-no-pt ftco-no-pb" style="background-image: url({{ asset('public/images/bg_8.jpg') }}" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-end">
          <div class="col-md-12 py-5 px-md-5">
            <div class="py-md-5">
              <div class="heading-section heading-section-white ftco-animate mb-5">
                <h2 class="mb-4">@lang('front.request_quote')</h2>
                
              </div>
              <form action="#" class="appointment-form ftco-animate">
                <div class="d-md-flex">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="{{ __('front.name') }}" id="quote_name">
                  </div>
                  <div class="form-group ml-md-4">
                    <input type="text" class="form-control" placeholder="{{ __('front.email') }}"  id="quote_email">
                  </div>
                </div>
                <div class="d-md-flex">
                  <div class="form-group">
                    <div class="form-field">
                      <div class="select-wrap">
                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                        <select name="" id="quote_service" class="form-control">
                          <option value="0" style="color: #afafaf">----- {{ __('front.select_service') }} -----</option>
                          @foreach($appServices as $service)
                            <option value="{{ $service->title_en }}">
                                @if($share_locale == 'ar')
                                  {{ $service->title_ar }}
                                @else
                                  {{ $service->title_en }}
                                @endif
                            </option>
                          @endforeach
                          <option value="Other">{{ __('front.other_service') }}</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group ml-md-4">
                    <input type="text" class="form-control" placeholder="{{ __('front.phone') }}" id="quote_phone">
                  </div>
                </div>
                <div class="d-md-flex">
                  <div class="form-group">
                    <textarea name="" id="quote_message" cols="30" rows="2" class="form-control" placeholder="{{ __('front.message') }}"></textarea>
                  </div>
                  <div class="form-group ml-md-4">
                    <input type="button" onclick="servicemail('contact')" value="{{ __('front.request_quote') }}" class="btn btn-white py-3 px-4">
                  </div>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

  <section class="ftco-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">@lang('front.blog')</h2>
          </div>
        </div>
        <div class="row">
          @foreach($posts as $post)
          <div class="col-md-6 col-lg-4 ftco-animate">
            <div class="blog-entry">
              <a href="{{ url('/post/'.$post->id) }}" class="block-20 d-flex align-items-end" style="background-image: url({{ $post->image ? asset($post->image) : asset('public/images/no-photo.png') }});">
                <div class="meta-date text-center p-2">
                  <span class="day">{{ Carbon::parse($post->updated_at)->format('d') }}</span>
                  <span class="mos">{{ Carbon::parse($post->updated_at)->format('M') }}</span>
                  <span class="yr">{{ Carbon::parse($post->updated_at)->format('Y') }}</span>
                </div>
              </a>
              <div class="text bg-white p-4">
                <h3 class="heading"><a href="{{ url('/post/'.$post->id) }}">{{ $post->title }}</a></h3>
                <p>{{ $post->short_desc ?? '.....' }}</p>
                <div class="d-flex align-items-center mt-4">
                  <p class="mb-0"><a href="{{ url('/post/'.$post->id) }}" class="btn btn-primary">@lang('front.more')</a></p>
                  
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
  </section>
@endsection
