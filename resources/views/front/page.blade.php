@extends('front.front')

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url({{ asset('public/images/bg_'.$bkg.'.jpg') }});">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-2 bread">{{ $data->title }}</h1>
        <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('/') }}">@lang('front.home') <i class="ion-ios-arrow-forward"></i></a></span> <span>{{ $data->title }} </span></p>
      </div>
    </div>
  </div>
</section>

<section class="ftco-section">
      <div class="container">
        <div class="row d-flex">
          
          <div class="col-md-12 wrap-about pr-md-4 ftco-animate">
            {!! $data->content !!}
          </div>
        </div>
      </div>
    </section>
    
    <section class="ftco-intro ftco-no-pb img" style="background-image: url(images/bg_3.jpg);">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-10 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-0">@lang('front.guidance')</h2>
          </div>
        </div>  
      </div>
    </section>

    <section class="ftco-counter" id="section-counter">
      <div class="container">
        <div class="row d-md-flex align-items-center justify-content-center">
          <div class="wrapper">
            <div class="row d-md-flex align-items-center">
              @foreach($counters as $counter)
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                  <div class="block-18">
                    <div class="icon"><span class="flaticon-doctor"></span></div>
                    <div class="text">
                      <strong class="number" data-number="{{ $counter->count }}">0</strong>
                      <span>{{ $counter->title }}</span>
                    </div>
                  </div>
                </div>
                @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>
@stop