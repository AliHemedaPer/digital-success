@extends('front.front')

@section('content')
<section class="hero-wrap hero-wrap-2"  style="background-image: url({{ asset('public/images/bg_'.$bkg.'.jpg') }});">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">{{ $service->title }}</h1>
            <p class="breadcrumbs">
            <span class="mr-2"><a href="{{ url('/') }}">  @lang('front.home') <i class="ion-ios-arrow-forward"></i></a></span>
            <span class="mr-2"><a href="{{ url('/services/'.$service->category) }}">  {{ $service->postCategory['name_'.$share_locale] }} <i class="ion-ios-arrow-forward"></i></a></span>
            <span>
              {{ $service->title }}
            </span>
           </p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 ftco-animate">
            <h2 class="mb-3">&raquo; {{ $service->title }}</h2>
            
            {!! $service->content !!}
            
            <div>
              <input type="hidden" class="form-control" id="quote_service" value="{{ $service->title_en }}">
              <div class="comment-form-wrap pt-5">
                <h3 class="mb-5 h4 font-weight-bold">@lang('front.request')</h3>
                <form action="#" class="p-5 bg-light">
                  <div class="form-group">
                    <label for="name">{{ __('front.name') }} *</label>
                    <input type="text" class="form-control" id="quote_name">
                  </div>
                  <div class="form-group">
                    <label for="email">{{ __('front.email') }} *</label>
                    <input type="email" class="form-control" id="quote_email">
                  </div>
                  <div class="form-group">
                    <label for="website">{{ __('front.phone') }}</label>
                    <input type="text" class="form-control" id="quote_phone">
                  </div>

                  <div class="form-group">
                    <label for="message">{{ __('front.message') }}</label>
                    <textarea name="" id="quote_message" cols="30" rows="3" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <input type="button" onclick="servicemail('service')" value="@lang('front.request')" class="btn py-3 px-4 btn-primary">
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@stop