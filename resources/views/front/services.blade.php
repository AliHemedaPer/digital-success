@extends('front.front')

@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url({{ asset('public/images/bg_'.$bkg.'.jpg') }});">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-2 bread">
          {{ $category->name }}
        </h1>
         <p class="breadcrumbs">
          <span class="mr-2"><a href="{{ url('/') }}">  @lang('front.home') <i class="ion-ios-arrow-forward"></i></a></span>
          <span class="mr-2">@lang('front.services') <i class="ion-ios-arrow-forward"></i></span>
          <span>
            {{ $category->name }}
          </span>
         </p>
      </div>
    </div>
  </div>
</section>

<section class="ftco-section">
  <div class="container">
    
    {!! $category->content !!}

    <div class="row">
      @foreach($appServices as $service)
        @if($service->category == $category->id)
        <div class="col-md-4">
          <h4>
            @if($share_locale == 'ar')
              {{ $service->title_ar }}
            @else
              {{ $service->title_en }}
            @endif
          </h4>
          <div class="project mb-4 img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url({{ $service->image ? asset($service->image) : asset('public/images/no-photo.png') }});">
            <div class="overlay"></div>
            <a href="{{ url('/service/'.$service->id) }}" class="btn-site d-flex align-items-center justify-content-center"><span class="icon-subdirectory_arrow_right"></span></a>
            <div class="text text-center p-4">
              <h3>
                <a href="{{ url('/service/'.$service->id) }}">
                  @if($share_locale == 'ar')
                    {{ $service->title_ar }}
                  @else
                    {{ $service->title_en }}
                  @endif
                </a>
              </h3>
              <span>
                {{ $category->name }}
              </span>
            </div>
          </div>
        </div>
        @endif
    @endforeach
    </div>
    <!-- <div class="row mt-5">
      <div class="col text-center">
        <div class="block-27">
          <ul>
            <li><a href="#">&lt;</a></li>
            <li class="active"><span>1</span></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&gt;</a></li>
          </ul>
        </div>
      </div>
    </div> -->
  </div>
</section>
@stop