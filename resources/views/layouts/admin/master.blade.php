<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-path" content="{{url('/')}}">
    <title>@yield('title')</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/components.min.css?i=1')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/colors.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/app.css')}}" rel="stylesheet" type="text/css">

    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('public/backend/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('public/backend/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->
    @stack('scripts')
    <script src="{{asset('public/backend/js/plugins/ui/perfect_scrollbar.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('public/backend/js/demo_pages/layout_fixed_sidebar_custom.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/notifications/pnotify.min.js')}}"></script>
    <script src="{{asset('public/backend/js/app.js')}}"></script>
    
    <!-- <script src="{{asset('public/js/application.js')}}"></script>
    <script src="{{asset('public/js/notification.js')}}?guard=admin"></script> -->

    <script src="{{asset('public/backend/js/plugins/notifications/jgrowl.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/notifications/noty.min.js')}}"></script>
    <script src="{{asset('public/backend/js/demo_pages/extra_jgrowl_noty.js')}}"></script>

    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <style type="text/css">
        @yield('styles')
    </style>
    
</head>

<body class="navbar-top">

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-light fixed-top">

    <!-- Header with logos -->
    <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
        <div class="navbar-brand navbar-brand-md" style="font-size: 14px;font-weight: bold;">
            
                <!-- <img src="{{asset('public/backend/images/logo_light.png')}}" alt=""> -->
                @if(auth('pharmacy')->check())
                    {{ studly_case(auth('pharmacy')->user()->name) }}
                @elseif(auth('admin')->check())
                    {{ studly_case(auth('admin')->user()->name) }}
                @endif
            
        </div>

        <div class="navbar-brand navbar-brand-xs">
            <!-- <a href="index.html" class="d-inline-block">
                <img src="{{asset('public/backend/images/logo_icon_light.png')}}" alt="">
            </a> -->
        </div>
    </div>
    <!-- /header with logos -->


    <!-- Mobile controls -->
    <div class="d-flex flex-1 d-md-none">
        <div class="navbar-brand mr-auto">
            <a href="index.html" class="d-inline-block">
                <img src="{{asset('public/backend/images/logo_dark.png')}}" alt="">
            </a>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>

        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
    <!-- /mobile controls -->


    <!-- Navbar content -->
    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav mr-md-auto">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-gear"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    
                    @if(auth('admin')->user()->canUpdate('settings'))
                        <a class="dropdown-item" href="{{ url('admin/settings') }}"><i class="icon-gear"></i> @lang('admin.accountSettings')</a>
                    @endif
                    <a class="dropdown-item" href="{{ url('admin/chpassword') }}"><i class="icon-lock"></i> @lang('admin.changePassword')</a>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <a class="dropdown-item" href="#"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> @lang('admin.logout')</a>


                </div>
            </li>
        </ul>
    </div>
    <!-- /navbar content -->

</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-fixed sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- Main navigation -->
            @include('layouts.admin.partials.nav-admin')
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        @yield('header')
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            @if (count($errors) > 0)
                <div class="alert bg-danger text-white alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                    @foreach ($errors->all() as $error)
                        <span class="font-weight-semibold">Oh snap!</span> {{ $error }}<br>
                    @endforeach
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                    <span class="font-weight-semibold">{{ session('success') }}!</span> 
                </div>
            @endif
    
            @yield('content')
        </div>
        <!-- /content area -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; 2019. <a href="#" target="_blank">{{ $appSettings->app_name }}</a>
                    </span>

            </div>
        </div>
        <!-- /footer -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
@yield('jquery')
<script type="text/javascript">

// function changeLang(lang){
//     let url = "{{ url('locale/')}}";
//     $.ajax({
//          headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         url: url+'/'+lang,
//         type: "get",
//         success: function (response) {
//             window.location.reload();
//         }
//     });
// }

</script>
</body>
</html>
