<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
   <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/components.min.css?i=1')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/colors.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/RTL/app.css')}}" rel="stylesheet" type="text/css">

    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('public/backend/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('public/backend/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->
    @stack('scripts')
    <script src="{{asset('public/backend/js/plugins/ui/perfect_scrollbar.min.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('public/backend/js/demo_pages/layout_fixed_sidebar_custom.js')}}"></script>
    <script src="{{asset('public/backend/js/app.js')}}"></script>
    <script src="{{asset('public/backend/js/plugins/forms/styling/uniform.min.js')}}"></script>
</head>

<body >

    @yield('content')

@yield('jquery')
</body>
</html>
