<div class="card card-sidebar-mobile">
    <ul class="nav nav-sidebar" data-nav-type="accordion">

        <!-- Main -->
        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">@lang('admin.main')</div> <i class="icon-menu" title="Main"></i></li>
        <li class="nav-item">
            <a href="{{ url('admin/home') }}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'home') !== false)) active @endif">
                <i class="icon-home4"></i>
                <span>@lang('admin.dashboard')</span>
            </a>
        </li>

        <li class="nav-item nav-item-submenu @if(strpos($_SERVER['REQUEST_URI'], '/edit?pst=page') !== false)) nav-item-open @endif">
            <a href="#" class="nav-link"><i class="icon-vcard"></i> <span>@lang('admin.about')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Layouts" @if(strpos($_SERVER['REQUEST_URI'], '/edit?pst=page') !== false)) style="display: block;" @endif>
                <li class="nav-item">
                    <a href="{{url('admin/posts/1/edit?pst=page')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '1/edit?pst=page') !== false)) active @endif"><i class="icon-quotes-left"></i> <span>من نحن</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/posts/2/edit?pst=page')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '2/edit?pst=page') !== false)) active @endif"><i class="icon-quotes-left"></i> <span>لماذا النجاح الرقمي؟</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/posts/3/edit?pst=page')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '3/edit?pst=page') !== false)) active @endif"><i class="icon-quotes-left"></i> <span>وسائل الدفع</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/posts/4/edit?pst=page')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '4/edit?pst=page') !== false)) active @endif"><i class="icon-quotes-left"></i> <span>سياسة الخصوصية</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/posts/5/edit?pst=page')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '5/edit?pst=page') !== false)) active @endif"><i class="icon-quotes-left"></i> <span>إتفاقية الخدمة</span></a>
                </li>
            </ul>
        </li>

        <li class="nav-item">
            <a href="{{url('admin/categories')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'categories') !== false)) active @endif"><i class="icon-grid2"></i> <span>@lang('admin.categories')</span></a>
        </li>
        
        <li class="nav-item">
            <a href="{{url('admin/posts?pst=service')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'service') !== false)) active @endif"><i class="icon-magic-wand2"></i> <span>@lang('admin.services')</span></a>
        </li>
       
        <li class="nav-item">
            <a href="{{url('admin/posts?pst=blog')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], 'blog') !== false)) active @endif"><i class="icon-stack-text"></i> <span>@lang('admin.blogs')</span></a>
        </li>
        
        <li class="nav-item">
            <a href="{{url('admin/clients')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/clients') !== false)) active @endif"><i class="icon-users"></i> <span> @lang('admin.clients')</span></a>
        </li>

        <li class="nav-item">
            <a href="{{url('admin/banners')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/banners') !== false)) active @endif"><i class="icon-images2"></i> <span> @lang('admin.banners')</span></a>
        </li>

        <li class="nav-item">
            <a href="{{url('admin/counters')}}" class="nav-link @if(strpos($_SERVER['REQUEST_URI'], '/counters') !== false)) active @endif"><i class="icon-list-numbered"></i> <span> @lang('admin.counters')</span></a>
        </li>
        
        <!-- /main -->

    </ul>
</div>
