<?php
Route::get('/home', 'Admin\\AdminController@home')->name('admin.home');
Route::get('/', 'Admin\\AdminController@home')->name('admin.home');

Route::match(['get', 'post'], '/chpassword', 'Admin\\AdminController@changePassword');

Route::group(['middleware' => ['auth'], 'rols' => ['settings' => ['update']]], function () {
    Route::match(['get', 'post'], '/settings', 'Admin\AdminController@appSettings');
});


/** Posts Route  */
Route::group(['middleware' => ['auth'], 
              'service'    => ['service' => ['read']],
              'page'       => ['page' => ['read']],
              'blog'       => ['blog' => ['read']],
              'course'     => ['course' => ['read']]
             ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['index']]);
});

Route::group(['middleware' => ['auth'], 
              'service'    => ['service' => ['create']],
              'page'       => ['page' => ['create']],
              'blog'       => ['blog' => ['create']],
              'course'     => ['course' => ['create']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['create']]);
});

Route::group(['middleware' => ['auth'], 
              'service'    => ['service' => ['create']],
              'page'       => ['page' => ['create']],
              'blog'       => ['blog' => ['create']],
              'course'     => ['course' => ['create']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['store']]);
});

Route::group(['middleware' => ['auth'], 
              'service'    => ['service' => ['update']],
              'page'       => ['page' => ['update']],
              'blog'       => ['blog' => ['update']],
              'course'     => ['course' => ['update']]
             ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['edit']]);
});

Route::group(['middleware' => ['auth'], 
              'service'    => ['service' => ['update']],
              'page'       => ['page' => ['update']],
              'blog'       => ['blog' => ['update']],
              'course'     => ['course' => ['update']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['update']]);
});

Route::group(['middleware' => ['auth'], 
              'service'    => ['service' => ['delete']],
              'page'       => ['page' => ['delete']],
              'blog'       => ['blog' => ['delete']],
              'course'     => ['course' => ['delete']]
          ], function () {
    Route::resource('/posts', 'Admin\PostController', ['only' => ['destroy']]);
});

// Route::group(['middleware' => ['auth']], function () {
//     Route::patch('/posts/toggle_status/{post}', 'Admin\PostController@toggleStatus');
// });


/** Categories Route  */
Route::group(['middleware' => ['auth'], 'rols' => ['category' => ['read']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['index']]);
});
Route::group(['middleware' => ['auth'], 'rols' => ['category' => ['create']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['create']]);
});
Route::group(['middleware' => ['auth'], 'rols' => ['category' => ['create']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['store']]);
});
Route::group(['middleware' => ['auth'], 'rols' => ['category' => ['update']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['edit']]);
});
Route::group(['middleware' => ['auth'], 'rols' => ['category' => ['delete']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['destroy']]);
});
Route::group(['middleware' => ['auth'], 'rols' => ['category' => ['update']]], function () {
    Route::resource('/categories', 'Admin\CategoryController', ['only' => ['update']]);
});


/** Admins Route  */
Route::group(['middleware' => ['auth'], 'rols' => ['admin' => ['read']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['index']]);
});

Route::group(['middleware' => ['auth'], 'rols' => ['admin' => ['create']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['create']]);
});

Route::group(['middleware' => ['auth'], 'rols' => ['admin' => ['create']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['store']]);
});

Route::group(['middleware' => ['auth'], 'rols' => ['admin' => ['update']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['edit']]);
});

Route::group(['middleware' => ['auth'], 'rols' => ['admin' => ['delete']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['destroy']]);
});

Route::group(['middleware' => ['auth'], 'rols' => ['admin' => ['update']]], function () {
    Route::patch('/admins/toggle_status/{admin}', 'Admin\AdminController@toggleStatus');
});

Route::group(['middleware' => ['auth'], 'rols' => ['admin' => ['update']]], function () {
    Route::resource('/admins', 'Admin\AdminController', ['only' => ['update']]);
});

Route::get('getcounts', 'Admin\\AdminController@getCounts');
Route::get('clients', 'Admin\\AdminController@clients');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/banners', 'Admin\BannerController');
    Route::resource('/counters', 'Admin\CountersController');
});

