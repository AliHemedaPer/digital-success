<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Guest Available Routes */
Route::get('/', 'FrontController@index')->name('home');
Route::get('/home', 'FrontController@index')->name('home');

Route::get('/pg/{page_key}', 'FrontController@page')->name('page');
Route::get('/services/{category}', 'FrontController@services')->name('services');
Route::get('/service/{service}', 'FrontController@service')->name('service');
Route::get('/contact', 'FrontController@contact')->name('contect');
Route::get('/blog', 'FrontController@blog')->name('blog');
Route::get('/post/{post}', 'FrontController@post')->name('post');

Route::get('locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
});
Route::post('/servicemail', 'FrontController@servicemail')->name('servicemail');
Route::post('/contactmail', 'FrontController@contactmail')->name('contactmail');



/** Admin Auth Routes */
Route::get('admin/login', 'V1\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'V1\Auth\AdminLoginController@login')->name('admin.login.submit');

Route::get('admin/reset', 'V1\Auth\AdminLoginController@resetPassword')->name('admin.reset');
Route::post('admin/reset', 'V1\Auth\AdminLoginController@resetPassword')->name('admin.reset.submit');
// Route::get('admin/register', 'V1\Auth\AdminRegisterController@showRegistrationForm')->name('admin.register');
// Route::post('admin/register', 'V1\Auth\AdminRegisterController@register')->name('admin.register.submit');
Route::post('admin/logout', 'V1\Auth\AdminLoginController@logout')->name('admin.logout');
